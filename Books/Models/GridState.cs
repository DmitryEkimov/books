﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Books.Models
{
    public class GridState
    {
        public string aaSorting;
        public int iDisplayLength;
        public int iDisplayStart ;
        public string trueData;
        public string Statuses;
        public string OrderTypes;
        public string[] Columns;
        public string EntityName;
        public string EntityNameRus;
    }
}