﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Books.Models
{
    public class InvoiceInput : INavModel
    {
        [DisplayName("Продавец:")]
        [UIHint("SellersTypeAhead")]
        public string Seller { get; set; } // Seller
        [DisplayName("Магазин:")]
        [UIHint("ShopsTypeAhead")]
        public string Shop { get; set; } // Shop
        [DisplayName("Заказ:")]
        public string OrderNum { get; set; } // OrderNum
        [DisplayName("Дата на чеке:")]
        [UIHint("Date")]
        public DateTime? Date { get; set; } // Date
        [DisplayName("Общая цена:")]
        public decimal? TotalPrice { get; set; } // TotalPrice
        [DisplayName("Валюта:")]
        [UIHint("CurrenciesDropDown")]
        public string Currency { get; set; } // Currency
        [DisplayName("Номер в выписке:")]
        public int? ExtractNum { get; set; } // ExtractNum
        [DisplayName("В евро:")]
        public decimal? Eur { get; set; } // EUR
        [DisplayName("В рублях:")]
        public decimal? Rur { get; set; } // RUR
        [DisplayName("Дата в выписке:")]
        [UIHint("Date")]
        public DateTime? DateWithdrawal { get; set; } // DateWithdrawal
        [DisplayName("Перевод:")]
        [DataType(DataType.MultilineText)]
        public string RussianDesc { get; set; } // RussianDesc
        [DisplayName("Временный номер в выписке:")]
        public int? AccountingId { get; set; } // AccountingID
        [DisplayName("Нет инвойса:")]
        //[UIHint("checkbox")]
        public bool NoInvoice { get; set; } // NoInvoice
        [DisplayName("Файл:")]
        public string File { get; set; } // File
        [ScaffoldColumn(false)]
        public int? PreviousRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int Id { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int? NextRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int FirstRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int LastRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public long RN { get; set; }
        [ScaffoldColumn(false)]
        public long Total { get; set; }
        [ScaffoldColumn(false)]
        public Books.Models.GridState Table { get; set; }
        [ScaffoldColumn(false)]
        public string EntityName { get; set; }
        [DisplayName("Копия файла")]
        [UIHint("FileUpload")]
        public int FileCopyExists { get; set; }
        [ScaffoldColumn(false)]
        public string ContentType { get; set; }
    }
}