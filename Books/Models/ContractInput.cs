﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Books.Models
{
    public class ContractInput : INavModel
    {
        [DisplayName("Дата")]
        [UIHint("Date")]
        public DateTime? Date { get; set; } // Date
        [DisplayName("Номер")]
        public string Num { get; set; } // Num
        [DisplayName("Партнер")]
        public string Partner { get; set; } // Partner
        [DisplayName("Содержание")]
        public string Subject { get; set; } // Subject
        [DisplayName("Цена")]
        public decimal? Value { get; set; } // Value
        [DisplayName("Дата истечения")]
        [UIHint("Date")]
        public DateTime? Expire { get; set; } // Expire
        [DisplayName("Комментарий")]
        public string Comm { get; set; } // Comm
        [ScaffoldColumn(false)]
        public int? PreviousRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int Id { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int? NextRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int FirstRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int LastRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public long RN { get; set; }
        [ScaffoldColumn(false)]
        public long Total { get; set; }
        [ScaffoldColumn(false)]
        public Books.Models.GridState Table { get; set; }
        [ScaffoldColumn(false)]
        public string EntityName { get; set; }
        [DisplayName("Копия файла")]
        [UIHint("FileUpload")]
        public int FileCopyExists { get; set; }
        [ScaffoldColumn(false)]
        public string ContentType { get; set; }
    }
}