﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Books.DataAccess;
using Books.DataAccess.Attributes;
using Omu.ValueInjecter;

namespace Books.Models
{
    public class WorksInput : INavModel
    {
        public WorksInput()
        {
            
        }
        public WorksInput(Works works)
        {
            this.InjectFrom(works);
        }
        [DisplayName("Статус:")]
        [UIHint("StatusesDropDown")]
        [Required]
        public string Status { get; set; } // Status
        [DisplayName("Участник:")]
        [UIHint("UsersDropDown")]
        //[Required]
        public string User { get; set; } // user
        [DisplayName("Тема:")]
        [UIHint("ThemeTypeAhead")]
        public string Theme { get; set; } // theme
        [UIHint("OrderTypesDropDown")]
        [DisplayName("Тип заказа:")]
        [Required]
        public string OrderType { get; set; } // OrderType
        [DisplayName("Биб-ка")]
        [UIHint("LibraryTypeAhead")]
        public string Library { get; set; } // Library
        [UIHint("FormatsDropDown")]
        [DisplayName("Носитель:")]
        [Required]
        public string Format { get; set; } // Format
        [UIHint("BibTypesDropDown")]
        [DisplayName("Тип записи:")]
        [Required]
        public string BibType { get; set; } // bibType
        [DisplayName("Автор:")]
        [UIHint("AuthorsTypeAhead")]
        public string Author { get; set; } // author
        [DisplayName("Ред:")]
        [UIHint("EditorsTypeAhead")]
        public string Editor { get; set; } // editor
        [DisplayName("Загл:")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string TitleBook { get; set; } // title
        [DisplayName("Источник:")]
        public string Booktitle { get; set; } // booktitle
        [DisplayName("Изд-во:")]
        [UIHint("PublisherTypeAhead")]
        public string Publisher { get; set; } // publisher
        [DisplayName("Журнал:")]
        [UIHint("JournalTypeAhead")]
        public string Journal { get; set; } // journal
        [DisplayName("Год:")]
        public string Year { get; set; } // year
        [DisplayName("Том:")]
        public string Volume { get; set; } // volume
        [DisplayName("Номер:")]
        public string Number { get; set; } // number
        [DisplayName("Глава:")]
        public string Chapter { get; set; } // chapter
        [DisplayName("Стр:")]
        public string Pp { get; set; } // pp
        [DisplayName("Серия:")]
        public string Series { get; set; } // series
        [DisplayName("Место:")]
        [UIHint("AddressTypeAhead")]
        public string Address { get; set; } // address
        [DisplayName("ISBN")]
        public string Isbn { get; set; } // ISBN
        [DisplayName("Код источника:")]
        public int? Source { get; set; } // source
        [DisplayName("Продавец:")]
        [UIHint("SellersTypeAhead")]
        public string Seller { get; set; } // seller
        [DisplayName("Ссылка:")]
        public string Link { get; set; } // link
        [DisplayName("Предвар. цена:")]
        public decimal? PrimaryPrice { get; set; } // PrimaryPrice
        [DisplayName("Валюта:")]
        [UIHint("CurrenciesDropDown")]
        public string Currency { get; set; } 
        [DisplayName("Скопированы с:")]
        public string CopyRange { get; set; } // copyRange
        [DisplayName("Заказ:")]
        [UIHint("Date")]
        public DateTime? Ordered { get; set; } // Ordered
        [DisplayName("Счет:")]
        [UIHint("InvoicesDropDown")]
        public int? Invoice { get; set; } // invoice
        [DisplayName("Договор:")]
        [UIHint("ContractsDropDown")]
        public int? Contract { get; set; } // contract
        [DisplayName("Чек:")]
        [UIHint("ChecksDropDown")]
        public int? Check { get; set; } // check
        [DisplayName("Число сс. на скан:")]
        public int? Count { get; set; } // count
        [DisplayName("Приход:")]
        [UIHint("Date")]
        public DateTime? Obtained { get; set; } // Obtained
        [DisplayName("Начато сканирование:")]
        [UIHint("Date")]
        public DateTime? Scanning { get; set; } 
        [DisplayName("Формат:")]
        [UIHint("SizesDropDown")]
        public string Size { get; set; } // size
        [DisplayName("Исходный файл:")]
        public string File { get; set; } // File
        [DisplayName("Готовый файл:")]
        public string Path { get; set; } // Path
        [DisplayName("допинф:")]
        [DataType(DataType.MultilineText)]
        public string Comm { get; set; } // Comm
        [ScaffoldColumn(false)]
        public int? PreviousRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int Id { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int? NextRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int FirstRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int LastRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public long RN { get; set; }
        [ScaffoldColumn(false)]
        public long Total { get; set; }
        [ScaffoldColumn(false)]
        public Books.Models.GridState Table { get; set; }
        [ScaffoldColumn(false)]
        public string EntityName { get; set; }
        [DisplayName("У кого:")]
        [UIHint("UsersDropDown")]
        public string WhereIs { get; set; } // WhereIs
    }
}