﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Books.Models
{
    public class CheckInput : INavModel
    {
        [DisplayName("Дата на чеке")]
        [UIHint("Date")]
        public DateTime? Date { get; set; } // Date
        [DisplayName("Номер чека")]
        public int? Num { get; set; } // Num
        [DisplayName("Орган-ция")]
        [UIHint("AgencyTypeAhead")]
        public string Agency { get; set; } // Agency
        [DisplayName("Цена")]
        public decimal? Value { get; set; } // Value
        [DisplayName("Номер. в выписке")]
        public int? ExtractNum { get; set; } // ExtractNum
        [DisplayName("Дата в выписке")]
        [UIHint("Date")]
        public DateTime? DateWithdrawal { get; set; } // DateWithdrawal
        [DisplayName("Плательщик")]
        [UIHint("UsersDropDown")]
        public string Payer { get; set; } // Payer
        [DisplayName("Оплачено")]
        //[UIHint("checkbox")]
        public bool IsPaid { get; set; } // offset
        [DisplayName("Файл")]
        public string File { get; set; } // File
        [ScaffoldColumn(false)]
        public int? PreviousRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int Id { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int? NextRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int FirstRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int LastRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public long RN { get; set; }
        [ScaffoldColumn(false)]
        public long Total { get; set; }
        [ScaffoldColumn(false)]
        public Books.Models.GridState Table { get; set; }
        [ScaffoldColumn(false)]
        public string EntityName { get; set; }
        [DisplayName("Копия файла")]
        [UIHint("FileUpload")]
        public int FileCopyExists { get; set; }
        [ScaffoldColumn(false)]
        public string ContentType { get; set; }
    }
}