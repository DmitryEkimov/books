﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Books.Models
{
    public interface INavModel
    {
        [ScaffoldColumn(false)]
         int? PreviousRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
         int Id { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
         int? NextRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
         int FirstRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
         int LastRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        long RN { get; set; }
        [ScaffoldColumn(false)]
        long Total { get; set; }
        [ScaffoldColumn(false)]
        Books.Models.GridState Table { get; set; }
        [ScaffoldColumn(false)]
        string EntityName { get; set; }
    }
}