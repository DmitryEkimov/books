﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Books.Models
{
    public class UserProfileInput : INavModel
    {
        //[Key]
        //public short UserId { get; set; }  //(Primary key)
        [Required]
        [Display(Name = "Логин пользователя")]
        public string UserName { get; set; }
        [Required]
        //[MinLength(6,ErrorMessage = "минимальная длина пароля 6 символов")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "EMail")]
        [Microsoft.Web.Mvc.EmailAddress]
        public string EMail { get; set; }
        [Display(Name = "Роль")]
        [Required]
        [UIHint("RolesDropDown")]
        public string Role { get; set; }
        [ScaffoldColumn(false)]
        public int? PreviousRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int Id { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int? NextRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int FirstRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public int LastRow { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]
        public long RN { get; set; }
        [ScaffoldColumn(false)]
        public long Total { get; set; }
        [ScaffoldColumn(false)]
        public Books.Models.GridState Table { get; set; }
        [ScaffoldColumn(false)]
        public string EntityName { get; set; }
    }
}