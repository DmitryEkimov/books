﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Books.DataAccess;
using Books.Helper;
using Books.Models;
using BootstrapMvcSample.Controllers;
using Datatables.Mvc;
using Models;
using Newtonsoft.Json;
using Omu.ValueInjecter;

namespace Books.Controllers
{
    [Authorize(Roles = "администратор,редактор")]
    public class InvoicesController : BootstrapBaseController
    {

        public InvoicesController(BooksDbContext context, ISessionState sessionstate)
            : base(context, sessionstate)
        {
        }

        public ActionResult Index()
        {
            HttpCookie myCookie = this.Request.Cookies["Invoices"];
            var model = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.Columns = new[] { "№", "Продавец","Магазин","Заказ","Дата на чеке","Общая цена","Валюта","Номер в вып","В евро","В рублях","Дата в выписке","Перевод","Врем. ном","Нет инвойса","Файл" };
            model.EntityName = "Invoices";
            model.EntityNameRus = "счет";
            return View(model);
        }

        public ActionResult GetDataTables(Datatables.Mvc.DataTable dataTable)
        {
            var query = _context.Invoices.AsQueryable();

            var totalcount = query.Count();
            if (!String.IsNullOrEmpty(dataTable.sSearch))
            {
                int intval;
                if (int.TryParse(dataTable.sSearch, out intval))
                {
                    query = query.Where(x => x.Id == intval);
                }
            }
            var filtercount = query.Count();

            if (dataTable.iSortCols != null && dataTable.iSortCols.Any())
                switch (dataTable.iSortCols[0])
                {
                   case 0:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Id) : query.OrderByDescending(x => x.Id);
                        break;
                   case 1:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Seller) : query.OrderByDescending(x => x.Seller);
                        break;
                   case 2:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Shop) : query.OrderByDescending(x => x.Shop);
                        break;
                   case 3:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.OrderNum) : query.OrderByDescending(x => x.OrderNum);
                        break;
                   case 4:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Date) : query.OrderByDescending(x => x.Date);
                        break;
                   case 5:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.TotalPrice) : query.OrderByDescending(x => x.TotalPrice);
                        break;
                   case 6:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Currency) : query.OrderByDescending(x => x.Currency);
                        break;
                   case 7:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.ExtractNum) : query.OrderByDescending(x => x.ExtractNum);
                        break;
                   case 8:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Eur) : query.OrderByDescending(x => x.Eur);
                        break;
                   case 9:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Rur) : query.OrderByDescending(x => x.Rur);
                        break;
                   case 10:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.DateWithdrawal) : query.OrderByDescending(x => x.DateWithdrawal);
                        break;
                   case 11:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.RussianDesc) : query.OrderByDescending(x => x.RussianDesc);
                        break;
                   case 12:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.AccountingId) : query.OrderByDescending(x => x.AccountingId);
                        break;
                   case 13:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.NoInvoice) : query.OrderByDescending(x => x.NoInvoice);
                        break;
                   case 14:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.File) : query.OrderByDescending(x => x.File);
                        break;
                    default:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Id) : query.OrderByDescending(x => x.Id);
                        break;
                }
            query = query.Skip(dataTable.iDisplayStart);
            if (dataTable.iDisplayLength > 0)
            {
                query = query.Take(dataTable.iDisplayLength);
            }
            var table = query.ToList().Select(t => new DataTableRow(t.Id.ToString(), "dtrowclass") { t.Id.ToString(), t.Seller, t.Shop, t.OrderNum, t.Date.HasValue ? t.Date.Value.ToShortDateString() : "", t.TotalPrice.ToString(), t.Currency.ToString(), t.ExtractNum.ToString(), t.Eur.ToString(), t.Rur.ToString(), t.DateWithdrawal.HasValue ? t.DateWithdrawal.Value.ToShortDateString() : "", t.RussianDesc, t.AccountingId.ToString(), t.NoInvoice?"1":"", t.File }).ToList();
            HttpCookie myCookie = this.Request.Cookies["Invoices"] ?? new HttpCookie("Invoices");
            myCookie.Value = JsonConvert.SerializeObject(new GridState() { iDisplayLength = dataTable.iDisplayLength, iDisplayStart = dataTable.iDisplayStart, aaSorting = "[[ " + dataTable.iSortCols[0] + ", \"" + (dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? "asc" : "desc") + "\" ]]" });
            myCookie.Expires = DateTime.Now.AddDays(365);
            return new DataTableResultExt(dataTable, totalcount, filtercount, table);
        }
        [HttpPost]
        public ActionResult Create(InvoiceInput model)
        {
            if (ModelState.IsValid)
            {
                var source = new Invoices();
                source.InjectFrom(model);
                if (Request.Files != null && Request.Files.Count > 0)//&& string.IsNullOrEmpty(headers["X-File-Name"]) 
                {
                    var file = Request.Files[0];
                    var FileCopy = file.InputStream.ToArray();
                    var ContentType = file.ContentType;
                    source.FileCopy = FileCopy;
                    source.ContentType = ContentType;
                }
                _context.Invoices.Add(source);
                _context.SaveChanges();
                Success("счет сохранен!");
                return RedirectToAction("Index");
            }
            Error("были ошибки в форме ввода.");
            ViewBag.Title = "Новый счет";
            model.Id = 0;
            model.FileCopyExists = 0;
            return View(model);
        }

        public ActionResult Create()
        {
            ViewBag.Title = "Новый счет";
            var model = new InvoiceInput {Id = 0, FileCopyExists = 0};
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var model = _context.Invoices.FirstOrDefault(w => w.Id == id);
            if(model==null)
                Attention("нет такого счета");
            else
            {
                var query = _context.Works.Where(x => x.Invoice == id);
                foreach (var bk in query)
                {
                    bk.Invoice = null;
                }
                _context.SaveChanges();
                _context.Invoices.Remove(model);
                _context.SaveChanges();
                Information("счет удален");
            }
            return RedirectToAction("index");
        }
        public ActionResult Edit(int id)
        {
            var source = _context.Invoices.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такого счета");
                return RedirectToAction("index");
            }
            var model = new InvoiceInput();
            model.InjectFrom(source);
            var worksprevnext = _context.InvoicesPrevNext.FirstOrDefault(w => w.Id == id);
            if (worksprevnext != null)
            {
                model.PreviousRow = worksprevnext.PreviousRow;
                model.NextRow = worksprevnext.NextRow;
                model.Id = worksprevnext.Id;
                model.RN = worksprevnext.RN;
                model.Total = _context.Invoices.Count();
            }
            model.FirstRow = _context.Invoices.Min(r => r.Id);
            model.LastRow = _context.Invoices.Max(r => r.Id);
            ViewBag.Title = "Счет № " + id;
            HttpCookie myCookie = this.Request.Cookies["BooksInInvoices"];
            model.Table = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.EntityName = "Invoices";
            model.FileCopyExists = source.FileCopy != null ? id : 0;
            return View("Create", model);
        }
        [HttpPost]
        public ActionResult Edit(InvoiceInput model, int id)
        {
            var source = _context.Invoices.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такого счета");
                return RedirectToAction("index");
            }
            if (ModelState.IsValid)
            {
                source.InjectFrom(model);
                source.Id = id;
                if (Request.Files != null && Request.Files.Count > 0)//&& string.IsNullOrEmpty(headers["X-File-Name"]) 
                {
                    var file = Request.Files[0];
                    var FileCopy = file.InputStream.ToArray();
                    var ContentType = file.ContentType;
                    source.FileCopy = FileCopy;
                    source.ContentType = ContentType;
                }
                _context.SaveChanges();
                Success("счет обновлен!");
                return RedirectToAction("index");
            }
            var worksprevnext = _context.InvoicesPrevNext.FirstOrDefault(w => w.Id == id);
            if (worksprevnext != null)
            {
                model.PreviousRow = worksprevnext.PreviousRow;
                model.NextRow = worksprevnext.NextRow;
                model.Id = worksprevnext.Id;
                model.RN = worksprevnext.RN;
                model.Total = _context.Invoices.Count();
            }
            model.FirstRow = _context.Invoices.Min(r => r.Id);
            model.LastRow = _context.Invoices.Max(r => r.Id);
            ViewBag.Title = "Счет № " + id;
            HttpCookie myCookie = this.Request.Cookies["BooksInInvoices"];
            model.Table = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.EntityName = "Invoices";
            model.FileCopyExists = source.FileCopy != null ? id : 0;
            return View("Create", model);
        }

        public ActionResult Details(int id)
        {
            var source = _context.Invoices.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такого счета");
                return RedirectToAction("index");
            }
            var model = new InvoiceInput();
            model.InjectFrom(source);
            return View(model);
        }
        public ActionResult GetBooksTable(int id, Datatables.Mvc.DataTable dataTable)
        {
            var query = _context.Works.AsQueryable();

            query = query.Where(x => x.Invoice == id);

            var totalcount = query.Count();
            if (User.IsInRole("пользователь"))
            {
                var user = User.Identity.Name;
                query = query.Where(x => x.User == user);
            }

            if (!String.IsNullOrEmpty(dataTable.sSearch))
            {
                int intval;
                query = int.TryParse(dataTable.sSearch, out intval) ? query.Where(x => x.Id == intval) : query.Where(x => x.Title.Contains(dataTable.sSearch) || x.Author.Contains(dataTable.sSearch) || x.Publisher.Contains(dataTable.sSearch) || x.Journal.Contains(dataTable.sSearch) || x.Chapter.Contains(dataTable.sSearch) || x.Series.Contains(dataTable.sSearch) || x.Booktitle.Contains(dataTable.sSearch));
            }
            if (!String.IsNullOrEmpty(dataTable.sSearchs[1]))
            {
                var searchterm = dataTable.sSearchs[1];
                query = query.Where(x => x.Status == searchterm);
            }
            if (!String.IsNullOrEmpty(dataTable.sSearchs[2]))
            {
                var searchterm = dataTable.sSearchs[2];
                query = query.Where(x => x.OrderType == searchterm);
            }
            var filtercount = query.Count();

            if (dataTable.iSortCols != null && dataTable.iSortCols.Any())
                switch (dataTable.iSortCols[0])
                {
                    case 0:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Id)
                                    : query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Status)
                                    : query.OrderByDescending(x => x.Status);
                        break;
                    case 2:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.OrderType)
                                    : query.OrderByDescending(x => x.OrderType);
                        break;
                    case 3:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Title)
                                    : query.OrderByDescending(x => x.Title);
                        break;
                    case 4:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Ordered)
                                    : query.OrderByDescending(x => x.Ordered);
                        break;
                    case 5:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Obtained)
                                    : query.OrderByDescending(x => x.Obtained);
                        break;
                    default:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Id)
                                    : query.OrderByDescending(x => x.Id);
                        break;
                }
            query = query.Skip(dataTable.iDisplayStart);
            if (dataTable.iDisplayLength > 0)
            {
                query = query.Take(dataTable.iDisplayLength);
            }
            var table =
                query.ToList()
                     .Select(
                         t =>
                         {
                             var worksInput = new WorksInput(t);
                             return new DataTableRow(t.Id.ToString(), "dtrowclass")
                                            {
                                                t.Id.ToString(),
                                                t.Status,
                                                t.OrderType,
                                                t.LibRef(),
                                                t.Ordered.HasValue
                                                    ? t.Ordered.Value
                                                       .ToShortDateString()
                                                    : "",
                                                t.Obtained.HasValue
                                                    ? t.Obtained.Value
                                                       .ToShortDateString()
                                                    : ""
                                            };
                         }).ToList();
            HttpCookie myCookie = this.Request.Cookies["BooksInInvoices"] ?? new HttpCookie("BooksInInvoices");
            myCookie.Value = JsonConvert.SerializeObject(new GridState() { iDisplayLength = dataTable.iDisplayLength, iDisplayStart = dataTable.iDisplayStart, aaSorting = "[[ " + dataTable.iSortCols[0] + ", \"" + (dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? "asc" : "desc") + "\" ]]" });
            myCookie.Expires = DateTime.Now.AddDays(365);
            this.Response.Cookies.Add(myCookie);
            return new DataTableResultExt(dataTable, totalcount, filtercount, table);
        }
        public ActionResult FileCopy(int id)
        {
            var source = _context.Invoices.FirstOrDefault(w => w.Id == id);
            if (source == null || source.FileCopy == null || String.IsNullOrWhiteSpace(source.ContentType))
            {
                return new EmptyResult();
            }
            return File(source.FileCopy, source.ContentType, "Счет_" + source.Id);
        }
        //[HttpPost]
        public ActionResult FileUpload(int? id, IEnumerable<HttpPostedFileBase> files)
        {
            //var headers = Request.Headers;
            if (files != null && files.Any())//&& string.IsNullOrEmpty(headers["X-File-Name"]) 
            {
                var file = files.First();
                var FileCopy = file.InputStream.ToArray();
                var ContentType = file.ContentType;

                if (id.HasValue)
                {
                    var source = _context.Invoices.FirstOrDefault(w => w.Id == id);
                    source.FileCopy = FileCopy;
                    source.ContentType = ContentType;
                    _context.SaveChanges();
                }
                else
                {
                    Session["FileCopy"] = FileCopy;
                    Session["ContentType"] = ContentType;
                }
                //else
                //{
                //    UploadPartialFile(headers["X-File-Name"], Request);
                //}
            }

            JsonResult result = Json("");
            result.ContentType = "text/plain";
            result.ContentEncoding = Encoding.UTF8;
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }
    }

}
