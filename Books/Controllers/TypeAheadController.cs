﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Books.DataAccess;
using Books.Helper;
using BootstrapMvcSample.Controllers;

namespace Books.Controllers
{
    public class TypeAheadController : BootstrapBaseController
    {
        public TypeAheadController(BooksDbContext context, ISessionState sessionstate)
            : base(context, sessionstate)
        {
        }
        public class Options
        {
            public IEnumerable<string> options;
        }
        public ActionResult Library(string query)
        {
            var result = new Options();
            result.options = _context.Works.Where(w => w.Library.StartsWith(query)).Select(w => w.Library).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Publisher(string query)
        {
            var result = new Options();
            result.options = _context.Works.Where(w => w.Publisher.StartsWith(query)).Select(w => w.Publisher).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Journal(string query)
        {
            var result = new Options();
            result.options = _context.Works.Where(w => w.Journal.StartsWith(query)).Select(w => w.Journal).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Theme(string query)
        {
            var result = new Options();
            result.options = _context.Works.Where(w => w.Theme.StartsWith(query)).Select(w => w.Theme).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Author(string query)
        {
            var result = new Options();
            result.options = _context.Works.Where(w => w.Author.StartsWith(query)).Select(w => w.Author).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Editor(string query)
        {
            var result = new Options();
            result.options = _context.Works.Where(w => w.Editor.StartsWith(query)).Select(w => w.Editor).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Seller(string query)
        {
            var result = new Options();
            result.options = _context.Works.Where(w => w.Seller.StartsWith(query)).Select(w => w.Seller).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Address(string query)
        {
            var result = new Options();
            result.options = _context.Works.Where(w => w.Address.StartsWith(query)).Select(w => w.Address).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Shop(string query)
        {
            var result = new Options();
            result.options = _context.Invoices.Where(w => w.Shop.StartsWith(query)).Select(w => w.Shop).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Agency(string query)
        {
            var result = new Options();
            result.options = _context.Checks.Where(w => w.Agency.StartsWith(query)).Select(w => w.Agency).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        
    }
}
