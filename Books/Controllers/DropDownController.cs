﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Books.DataAccess;
using Books.Helper;
using BootstrapMvcSample.Controllers;

namespace Books.Controllers
{
    public class DropDownController : BootstrapBaseController
    {
        public DropDownController(BooksDbContext context, ISessionState sessionstate)
            : base(context, sessionstate)
        {
        }

        public ActionResult GetStatuses(string selectedId)
        {
            ViewData.Model = _context.Statuses.Select(c=>new SelectListItem()
                                    {
                                        Value = c.Status,
                                        Text = c.Status,
                                        Selected = selectedId==c.Status
                                    }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Status") { NullDisplayText = " выберите статус" };
            return View("Dropdown");
        }

        public ActionResult GetOrderTypes(string selectedId)
        {
            ViewData.Model = _context.OrderTypes.Select(c=>new SelectListItem()
                                    {
                                        Value = c.OrderType,
                                        Text = c.OrderType,
                                        Selected = selectedId==c.OrderType
                                    }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "OrderType") { NullDisplayText = " выберите Тип заказа" };
            return View("Dropdown");
        }

        public ActionResult GetFormats(string selectedId)
        {
            ViewData.Model = _context.Formats.Select(c=>new SelectListItem()
                                    {
                                        Value = c.Format,
                                        Text = c.Format,
                                        Selected = selectedId==c.Format
                                    }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Format") { NullDisplayText = " выберите Носитель" };
            return View("Dropdown");
        }

        public ActionResult GetCurrencies(string selectedId)
        {
            ViewData.Model = _context.Currencies.Select(c=>new SelectListItem()
                                    {
                                        Value = c.Currency,
                                        Text = c.Currency,
                                        Selected = selectedId==c.Currency
                                    }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Currency") { NullDisplayText = " выберите валюту " };
            return View("Dropdown");
        }

        public ActionResult GetBibTypes(string selectedId)
        {
            ViewData.Model = _context.BibTypes.Select(c => new SelectListItem()
            {
                Value = c.BibType,
                Text = c.BibType,
                Selected = selectedId == c.BibType
            }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "BibType") { NullDisplayText = " выберите тип записи" };
            return View("Dropdown");
        }
        public ActionResult GetSizes(string selectedId)
        {
            //9. Формат -- выпадающий списко из двух значений А4 и А3
            var values = new[] {"A4", "A3"};
            ViewData.Model = values.Select(c => new SelectListItem()
            {
                Value = c,
                Text = c,
                Selected = selectedId == c
            }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Size") { NullDisplayText = " выберите формат" };
            return View("Dropdown");
        }
        public ActionResult GetUsers(string selectedId)
        {
            ViewData.Model = _context.UserProfiles.Select(c => new SelectListItem()
            {
                Value = c.UserName,
                Text = c.UserName,
                Selected = selectedId == c.UserName
            }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "User") { NullDisplayText = " выберите пользователя" };
            return View("Dropdown");
        }

        public ActionResult GetChecks(int selectedId)
        {
            ViewData.Model = _context.Checks.Select(inv => inv.Id).ToList().Select(c => new SelectListItem()
            {
                Value = c.ToString(),
                Text = c.ToString(),
                Selected = selectedId == c
            }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Check") { NullDisplayText = " выберите чек" };
            return View("Dropdown");
        }

        public ActionResult GetContracts(int selectedId)
        {
            ViewData.Model = _context.Contracts.ToList().Select(c => new SelectListItem()
            {
                Value = c.Id.ToString(),
                Text = c.Id.ToString()+(c.Date.HasValue?" от "+c.Date.Value.ToShortDateString():"")+" "+c.Partner,
                Selected = selectedId == c.Id
            }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Contract") { NullDisplayText = " выберите договор" };
            return View("Dropdown");
        }

        public ActionResult GetInvoices(int selectedId)
        {
            ViewData.Model = _context.Invoices.Select(inv=>inv.Id).ToList().Select(c => new SelectListItem()
            {
                Value = c.ToString(),
                Text = c.ToString(),
                Selected = selectedId == c
            }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Invoice") { NullDisplayText = " выберите счет" };
            return View("Dropdown");
        }

        public ActionResult GetRoles(string selectedId)
        {
            ViewData.Model = Roles.GetAllRoles().Select(c => new SelectListItem()
            {
                Value = c,
                Text = c,
                Selected = selectedId == c
            }).ToList();

            ViewData.ModelMetadata =
                new ModelMetadata(
                    ModelMetadataProviders.Current,
                    null,
                    null,
                    typeof(string),
                    "Role") { NullDisplayText = " выберите роль" };
            return View("Dropdown");
        }
            //HasOptional(a => a.Invoices).WithMany(b => b.Works).HasForeignKey(c => c.Invoice); // FK_works_invoice
            //HasOptional(a => a.Contracts).WithMany(b => b.Works).HasForeignKey(c => c.Contract); // FK_works_contract
            //HasOptional(a => a.Checks).WithMany(b => b.Works).HasForeignKey(c => c.Check); // FK_works_check
            //HasOptional(a => a.Acts).WithMany(b => b.Works).HasForeignKey(c => c.Act); // FK_works_act
    }

}
