﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Books.DataAccess;
using Books.Filters;
using Books.Helper;
using Books.Mailer;
using Books.Models;
using BootstrapMvcSample.Controllers;
using Datatables.Mvc;
using Models;
using Newtonsoft.Json;
using Omu.ValueInjecter;

namespace Books.Controllers
{
    [Authorize]
    //[InitializeSimpleMembership]
    public class BooksController : BootstrapBaseController
    {
        private IUserMailer _mailer;
        public BooksController(BooksDbContext context, ISessionState sessionstate, IUserMailer mailer)
            : base(context, sessionstate)
        {
            _mailer = mailer;
        }

        public ActionResult Index()
        {
            HttpCookie myCookie = this.Request.Cookies["Books"] ;
            var model = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.Statuses = JsonConvert.SerializeObject(_context.Statuses.Select(c => c.Status).ToArray());
            model.OrderTypes = JsonConvert.SerializeObject(_context.OrderTypes.Select(c => c.OrderType).ToArray());
            model.Columns = new[] {"№","Статус", "Тип з-за", "Описание", "Зак-но", "Получено"};
            model.EntityName = "Books";
            model.EntityNameRus = "книгу";

            return View(model);
        }

        public ActionResult GetDataTables(Datatables.Mvc.DataTable dataTable)
        {
            var query = _context.Works.AsQueryable();

            var totalcount = query.Count();
            if (User.IsInRole("пользователь"))
            {
                var user = User.Identity.Name;
                query = query.Where(x => x.User == user);
            }

            if (!String.IsNullOrEmpty(dataTable.sSearch))
            {
                int intval;
                query = int.TryParse(dataTable.sSearch, out intval) ? query.Where(x => x.Id == intval) : query.Where(x => x.Title.Contains(dataTable.sSearch) || x.Author.Contains(dataTable.sSearch) || x.Publisher.Contains(dataTable.sSearch) || x.Journal.Contains(dataTable.sSearch) || x.Chapter.Contains(dataTable.sSearch) || x.Series.Contains(dataTable.sSearch) || x.Booktitle.Contains(dataTable.sSearch));
            }
            if (!String.IsNullOrEmpty(dataTable.sSearchs[1]))
            {
                var searchterm = dataTable.sSearchs[1];
                query = query.Where(x => x.Status == searchterm);
            }
            if (!String.IsNullOrEmpty(dataTable.sSearchs[2]))
            {
                var searchterm = dataTable.sSearchs[2];
                query = query.Where(x => x.OrderType == searchterm);
            }
            var filtercount = query.Count();

            if (dataTable.iSortCols != null && dataTable.iSortCols.Any())
                switch (dataTable.iSortCols[0])
                {
                    case 0:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Id)
                                    : query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Status)
                                    : query.OrderByDescending(x => x.Status);
                        break;
                    case 2:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.OrderType)
                                    : query.OrderByDescending(x => x.OrderType);
                        break;
                    case 3:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Title)
                                    : query.OrderByDescending(x => x.Title);
                        break;
                    case 4:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Ordered)
                                    : query.OrderByDescending(x => x.Ordered);
                        break;
                    case 5:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Obtained)
                                    : query.OrderByDescending(x => x.Obtained);
                        break;
                    default:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Id)
                                    : query.OrderByDescending(x => x.Id);
                        break;
                }
            query = query.Skip(dataTable.iDisplayStart);
            if (dataTable.iDisplayLength > 0)
            {
                query = query.Take(dataTable.iDisplayLength);
            }
            var table =
                query.ToList()
                     .Select(
                         t =>
                             {
                                 var worksInput = new WorksInput(t);
                                 return new DataTableRow(t.Id.ToString(), "dtrowclass")
                                            {
                                                t.Id.ToString(),
                                                t.Status,
                                                t.OrderType,
                                                t.LibRef(),
                                                t.Ordered.HasValue
                                                    ? t.Ordered.Value
                                                       .ToShortDateString()
                                                    : "",
                                                t.Obtained.HasValue
                                                    ? t.Obtained.Value
                                                       .ToShortDateString()
                                                    : ""
                                            };
                             }).ToList();
            HttpCookie myCookie = this.Request.Cookies["Books"] ?? new HttpCookie("Books");
            myCookie.Value=JsonConvert.SerializeObject(new GridState() { iDisplayLength = dataTable.iDisplayLength, iDisplayStart = dataTable.iDisplayStart, aaSorting = "[[ " + dataTable.iSortCols[0] + ", \"" + (dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? "asc" : "desc") + "\" ]]" });
            myCookie.Expires = DateTime.Now.AddDays(365);
            this.Response.Cookies.Add(myCookie);
            return new DataTableResultExt(dataTable, totalcount, filtercount, table);
        }
        [HttpPost]
        public ActionResult Create(WorksInput model)
        {
            if (ModelState.IsValid)
            {
                var source = new Works();
                source.InjectFrom(model);
                source.Title = model.TitleBook;
                source.CreatedOn = DateTime.Now;
                _context.Works.Add(source);
                _context.SaveChanges();
                Success("книга сохранена!");
                return RedirectToAction("Index");
            }
            Error("были ошибки в форме ввода.");
            ViewBag.CreatedOn = DateTime.Now.ToShortDateString();
            ViewBag.Title = "";
            return View(model);
        }

        public ActionResult Create()
        {
            ViewBag.CreatedOn = DateTime.Now.ToShortDateString();
            ViewBag.Title = "";
            var model = new WorksInput { Status = "На заказ", OrderType = "BookOrder", PreviousRow = null, NextRow = null, Id = 0, Format = "paperBook", BibType="book", User = User.Identity.Name };
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var model = _context.Works.FirstOrDefault(w => w.Id == id);
            if(model==null)
                Attention("нет такой книги");
            else
            {
                _context.Works.Remove(model);
                _context.SaveChanges();
                Information("книга удалена");
            }
            return RedirectToAction("index");
        }
        public ActionResult Edit(int id)
        {
            var source = _context.Works.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такой книги");
                return RedirectToAction("index");
            }
            var model = new WorksInput();
            model.InjectFrom(source);
            model.TitleBook = source.Title;
            ViewBag.CreatedOn = source.CreatedOn.HasValue?source.CreatedOn.Value.ToShortDateString():"";
            ViewBag.Title = id + ". " + source.LibRef();
            var worksprevnext = _context.WorksPrevNext.FirstOrDefault(w => w.Id == id);
            if (worksprevnext != null)
            {
                model.PreviousRow = worksprevnext.PreviousRow;
                model.NextRow = worksprevnext.NextRow;
                model.Id = worksprevnext.Id;
                model.RN = worksprevnext.RN;
                model.Total = _context.Works.Count();
            }
            model.FirstRow = _context.Works.Min(r => r.Id);
            model.LastRow = _context.Works.Max(r => r.Id);
            model.EntityName = "Books";
            return View("Create", model);
        }
        [HttpPost]
        public ActionResult Edit(WorksInput model, int id)
        {
            var source = _context.Works.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такой книги");
                return RedirectToAction("index");
            }
            if (ModelState.IsValid )
            {
                var previousStatus = source.Status;
                source.InjectFrom(model);
                source.Title = model.TitleBook;
                source.Id = id;
                _context.SaveChanges();
                Success("книга обновлена!");

                if (previousStatus != model.Status)
                {
                    try
                    {
                        var username = source.User;
                        var email = _context.UserProfiles.Single(u => u.UserName == username).EMail;
                        if (!string.IsNullOrWhiteSpace(email)&& id>0)
                        {
                            switch (model.Status)
                            {
                                case "Заказано":
                                    _mailer.SendЗаказано(email, id);
                                    break;
                                case "Получено":
                                    _mailer.SendПолучено(email, id);
                                    break;
                                case "Обработано":
                                    _mailer.SendОбработано(email, id);
                                    break;
                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
                 
                return RedirectToAction("index");
            }
            ViewBag.CreatedOn = source.CreatedOn.HasValue ? source.CreatedOn.Value.ToShortDateString() : "";
            ViewBag.Title = id + ". " + source.LibRef();
            var worksprevnext = _context.WorksPrevNext.FirstOrDefault(w => w.Id == id);
            if (worksprevnext != null)
            {
                model.PreviousRow = worksprevnext.PreviousRow;
                model.NextRow = worksprevnext.NextRow;
                model.Id = worksprevnext.Id;
                model.RN = worksprevnext.RN;
                model.Total = _context.Works.Count();
            }
            model.FirstRow = _context.Works.Min(r => r.Id);
            model.LastRow = _context.Works.Max(r => r.Id);
            model.EntityName = "Books";
            return View("Create", model);
        }

        public ActionResult Details(int id)
        {
            var source = _context.Works.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такой книги");
                return RedirectToAction("index");
            }
            var model = new WorksInput();
            model.InjectFrom(source);
            return View(model);
        }

    }

}
