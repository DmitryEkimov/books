﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Books.DataAccess;
using Books.Helper;
using Books.Models;
using BootstrapMvcSample.Controllers;
using Datatables.Mvc;
using Models;
using Newtonsoft.Json;
using Omu.ValueInjecter;

namespace Books.Controllers
{
    [Authorize(Roles = "администратор,редактор")]
    public class ContractsController : BootstrapBaseController
    {

        public ContractsController(BooksDbContext context, ISessionState sessionstate)
            : base(context, sessionstate)
        {
        }

        public ActionResult Index()
        {
            HttpCookie myCookie = this.Request.Cookies["Contracts"];
            var model = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.Columns = new[] { "№", "Дата", "Номер", "Партнер", "Содержание", "Цена", "Дата истечения", "Комментарий" };
            model.EntityName = "Contracts";
            model.EntityNameRus = "договор";
            return View(model);
        }

        public ActionResult GetDataTables(Datatables.Mvc.DataTable dataTable)
        {
            var query = _context.Contracts.AsQueryable();
            //var query = from hotel in context.Hotels
            //join categories in context.HotelCategories on hotel.HotelId equals categories.HotelId into hc
            //from x in hc.DefaultIfEmpty()
            //join packageBookings in context.PackageBookings on x.HotelCategoryId equals
            //    packageBookings.HotelCategoryId into xp
            //from y in xp.DefaultIfEmpty()
            //group y by new { hotel.HotelId , hotel.Name } into g
            //select new QueryView() { HotelId = g.Key.HotelId, Name=g.Key.Name, Totalfree = (g.Sum(t => t.BoughtsNumber) + g.Sum(t => t.ReservesNumber)), Totalbusy = 0 };

            var totalcount = query.Count();
            if (!String.IsNullOrEmpty(dataTable.sSearch))
            {
                int intval;
                if (int.TryParse(dataTable.sSearch, out intval))
                {
                    query = query.Where(x => x.Id == intval);
                }
                else
                {
                    query = query.Where(x => x.Subject.StartsWith(dataTable.sSearch)|| x.Num.StartsWith(dataTable.sSearch));
                }
            }
            var filtercount = query.Count();

            if (dataTable.iSortCols != null && dataTable.iSortCols.Any())
                switch (dataTable.iSortCols[0])
                {
                    case 0:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Id) : query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Date) : query.OrderByDescending(x => x.Date);
                        break;
                    case 2:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Num) : query.OrderByDescending(x => x.Num);
                        break;
                    case 3:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Partner) : query.OrderByDescending(x => x.Partner);
                        break;
                    case 4:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Subject) : query.OrderByDescending(x => x.Subject);
                        break;
                    case 5:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Value) : query.OrderByDescending(x => x.Value);
                        break;
                    case 6:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Expire) : query.OrderByDescending(x => x.Expire);
                        break;
                    case 7:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Comm) : query.OrderByDescending(x => x.Comm);
                        break;
                    default:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? query.OrderBy(x => x.Id) : query.OrderByDescending(x => x.Id);
                        break;
                }
            query = query.Skip(dataTable.iDisplayStart);
            if (dataTable.iDisplayLength > 0)
            {
                query = query.Take(dataTable.iDisplayLength);
            }
            var table = query.ToList().Select(t => new DataTableRow(t.Id.ToString(), "dtrowclass") { t.Id.ToString(), t.Date.HasValue ? t.Date.Value.ToShortDateString() : "", t.Num, t.Partner, t.Subject, t.Value.ToString(), t.Expire.HasValue ? t.Expire.Value.ToShortDateString() : "", t.Comm }).ToList();
            HttpCookie myCookie = this.Request.Cookies["Contracts"] ?? new HttpCookie("Contracts");
            myCookie.Value = JsonConvert.SerializeObject(new GridState() { iDisplayLength = dataTable.iDisplayLength, iDisplayStart = dataTable.iDisplayStart, aaSorting = "[[ " + dataTable.iSortCols[0] + ", \"" + (dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? "asc" : "desc") + "\" ]]" });
            myCookie.Expires = DateTime.Now.AddDays(365);
            return new DataTableResultExt(dataTable, totalcount, filtercount, table);
        }
        [HttpPost]
        public ActionResult Create(ContractInput model)
        {
            if (ModelState.IsValid)
            {
                var source = new Contracts();
                source.InjectFrom(model);
                if (Request.Files != null && Request.Files.Count>0)//&& string.IsNullOrEmpty(headers["X-File-Name"]) 
                {
                    var file = Request.Files[0];
                    var FileCopy = file.InputStream.ToArray();
                    var ContentType = file.ContentType;
                    source.FileCopy = FileCopy;
                    source.ContentType = ContentType;
                }
                //source.CreatedOn = DateTime.Now;
                if (Session["FileCopy"] != null)
                    source.FileCopy = Session["FileCopy"] as byte[];
                if (Session["ContentType"] != null)
                    source.ContentType = Session["ContentType"] as string; 
                _context.Contracts.Add(source);
                _context.SaveChanges();
                Success("договор сохранен!");
                return RedirectToAction("Index");
            }
            Error("были ошибки в форме ввода.");
            ViewBag.Title = "Новый договор";
            model.Id = 0;
            model.FileCopyExists = 0;
            return View(model);
        }

        public ActionResult Create()
        {
            ViewBag.Title = "Новый договор";
            var model = new ContractInput {Id = 0, FileCopyExists = 0};
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var model = _context.Contracts.FirstOrDefault(w => w.Id == id);
            if(model==null)
                Attention("нет такого договора");
            else
            {
                var query = _context.Works.Where(x => x.Contract == id);
                foreach (var bk in query)
                {
                    bk.Contract = null;
                }
                _context.SaveChanges();
                _context.Contracts.Remove(model);
                _context.SaveChanges();
                Information("договор удален");
            }
            return RedirectToAction("index");
        }
        public ActionResult Edit(int id)
        {
            var source = _context.Contracts.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такого договора");
                return RedirectToAction("index");
            }
            var model = new ContractInput();
            model.InjectFrom(source);
            var worksprevnext = _context.ContractsPrevNext.FirstOrDefault(w => w.Id == id);
            if (worksprevnext != null)
            {
                model.PreviousRow = worksprevnext.PreviousRow;
                model.NextRow = worksprevnext.NextRow;
                model.Id = worksprevnext.Id;
                model.RN = worksprevnext.RN;
                model.Total = _context.Contracts.Count();
            }
            model.FirstRow = _context.Contracts.Min(r => r.Id);
            model.LastRow = _context.Contracts.Max(r => r.Id);
            ViewBag.Title = "Договор № " + id;
            HttpCookie myCookie = this.Request.Cookies["BooksInContract"];
            model.Table = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.EntityName = "Contracts";
            model.FileCopyExists = source.FileCopy!=null?id:0;
            return View("Create", model);
        }
        [HttpPost]
        public ActionResult Edit(ContractInput model, int id)
        {
            var source = _context.Contracts.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такого договора");
                return RedirectToAction("index");
            }
            if (ModelState.IsValid)
            {
                source.InjectFrom(model);
                source.Id = id;
                if (Request.Files != null && Request.Files.Count > 0)//&& string.IsNullOrEmpty(headers["X-File-Name"]) 
                {
                    var file = Request.Files[0];
                    var FileCopy = file.InputStream.ToArray();
                    var ContentType = file.ContentType;
                    source.FileCopy = FileCopy;
                    source.ContentType = ContentType;
                }
                _context.SaveChanges();
                Success("договор обновлен!");
                return RedirectToAction("index");
            }
            var worksprevnext = _context.ContractsPrevNext.FirstOrDefault(w => w.Id == id);
            if (worksprevnext != null)
            {
                model.PreviousRow = worksprevnext.PreviousRow;
                model.NextRow = worksprevnext.NextRow;
                model.Id = worksprevnext.Id;
                model.RN = worksprevnext.RN;
                model.Total = _context.Contracts.Count();
            }
            model.FirstRow = _context.Contracts.Min(r => r.Id);
            model.LastRow = _context.Contracts.Max(r => r.Id);
            ViewBag.Title = "Договор № " + id;
            HttpCookie myCookie = this.Request.Cookies["BooksInContract"];
            model.Table = myCookie != null ? JsonConvert.DeserializeObject<GridState>(myCookie.Value) : new GridState() { iDisplayStart = 0, iDisplayLength = 20, aaSorting = "[[ 0, \"asc\" ]]" };
            model.EntityName = "Contracts";
            model.FileCopyExists = source.FileCopy != null ? id : 0;
            return View("Create", model);
        }

        public ActionResult Details(int id)
        {
            var source = _context.Contracts.FirstOrDefault(w => w.Id == id);
            if (source == null)
            {
                Attention("нет такого договора");
                return RedirectToAction("index");
            }
            var model = new ContractInput();
            model.InjectFrom(source);
            return View(model);
        }
        public ActionResult GetBooksTable(int id, Datatables.Mvc.DataTable dataTable)
        {
            var query = _context.Works.AsQueryable();

            query = query.Where(x => x.Contract == id);

            var totalcount = query.Count();
            if (User.IsInRole("пользователь"))
            {
                var user = User.Identity.Name;
                query = query.Where(x => x.User == user);
            }

            if (!String.IsNullOrEmpty(dataTable.sSearch))
            {
                int intval;
                query = int.TryParse(dataTable.sSearch, out intval) ? query.Where(x => x.Id == intval) : query.Where(x => x.Title.Contains(dataTable.sSearch) || x.Author.Contains(dataTable.sSearch) || x.Publisher.Contains(dataTable.sSearch) || x.Journal.Contains(dataTable.sSearch) || x.Chapter.Contains(dataTable.sSearch) || x.Series.Contains(dataTable.sSearch) || x.Booktitle.Contains(dataTable.sSearch));
            }
            if (!String.IsNullOrEmpty(dataTable.sSearchs[1]))
            {
                var searchterm = dataTable.sSearchs[1];
                query = query.Where(x => x.Status == searchterm);
            }
            if (!String.IsNullOrEmpty(dataTable.sSearchs[2]))
            {
                var searchterm = dataTable.sSearchs[2];
                query = query.Where(x => x.OrderType == searchterm);
            }
            var filtercount = query.Count();

            if (dataTable.iSortCols != null && dataTable.iSortCols.Any())
                switch (dataTable.iSortCols[0])
                {
                    case 0:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Id)
                                    : query.OrderByDescending(x => x.Id);
                        break;
                    case 1:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Status)
                                    : query.OrderByDescending(x => x.Status);
                        break;
                    case 2:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.OrderType)
                                    : query.OrderByDescending(x => x.OrderType);
                        break;
                    case 3:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Title)
                                    : query.OrderByDescending(x => x.Title);
                        break;
                    case 4:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Ordered)
                                    : query.OrderByDescending(x => x.Ordered);
                        break;
                    case 5:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Obtained)
                                    : query.OrderByDescending(x => x.Obtained);
                        break;
                    default:
                        query = dataTable.sSortDirs[0] == DataTableSortDirection.Ascending
                                    ? query.OrderBy(x => x.Id)
                                    : query.OrderByDescending(x => x.Id);
                        break;
                }
            query = query.Skip(dataTable.iDisplayStart);
            if (dataTable.iDisplayLength > 0)
            {
                query = query.Take(dataTable.iDisplayLength);
            }
            var table =
                query.ToList()
                     .Select(
                         t =>
                         {
                             var worksInput = new WorksInput(t);
                             return new DataTableRow(t.Id.ToString(), "dtrowclass")
                                            {
                                                t.Id.ToString(),
                                                t.Status,
                                                t.OrderType,
                                                t.LibRef(),
                                                t.Ordered.HasValue
                                                    ? t.Ordered.Value
                                                       .ToShortDateString()
                                                    : "",
                                                t.Obtained.HasValue
                                                    ? t.Obtained.Value
                                                       .ToShortDateString()
                                                    : ""
                                            };
                         }).ToList();
            HttpCookie myCookie = this.Request.Cookies["BooksInContract"] ?? new HttpCookie("BooksInContract");
            myCookie.Value = JsonConvert.SerializeObject(new GridState() { iDisplayLength = dataTable.iDisplayLength, iDisplayStart = dataTable.iDisplayStart, aaSorting = "[[ " + dataTable.iSortCols[0] + ", \"" + (dataTable.sSortDirs[0] == DataTableSortDirection.Ascending ? "asc" : "desc") + "\" ]]" });
            myCookie.Expires = DateTime.Now.AddDays(365);
            this.Response.Cookies.Add(myCookie);
            return new DataTableResultExt(dataTable, totalcount, filtercount, table);
        }
        public ActionResult FileCopy(int id)
        {
            var source = _context.Contracts.FirstOrDefault(w => w.Id == id);
            if (source == null || source.FileCopy == null ||String.IsNullOrWhiteSpace(source.ContentType))
            {
                return new EmptyResult();
            }
            return File(source.FileCopy, source.ContentType, "Договор_" + source.Id);
        }
        //[HttpPost]
        public ActionResult FileUpload(int? id, IEnumerable<HttpPostedFileBase> files)
        {
            //var headers = Request.Headers;
            if (files != null && files.Any())//&& string.IsNullOrEmpty(headers["X-File-Name"]) 
            {
                var file = files.First();
                var FileCopy = file.InputStream.ToArray();
                var ContentType = file.ContentType;

                if (id.HasValue)
                {
                    var source = _context.Contracts.FirstOrDefault(w => w.Id == id);
                    source.FileCopy = FileCopy;
                    source.ContentType = ContentType;
                    _context.SaveChanges();
                }
                else
                {
                    Session["FileCopy"] = FileCopy;
                    Session["ContentType"] = ContentType;
                }
                //else
                //{
                //    UploadPartialFile(headers["X-File-Name"], Request);
                //}
            }

            JsonResult result = Json("");
            result.ContentType = "text/plain";
            result.ContentEncoding = Encoding.UTF8;
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }
    }

}
