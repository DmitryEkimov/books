﻿using System.Web.Mvc;
using Books.DataAccess;
using Books.Helper;
using BootstrapSupport;

namespace BootstrapMvcSample.Controllers
{
    public class BootstrapBaseController: Controller
    {
        protected readonly BooksDbContext _context;
        protected ISessionState _sessionstate;
        public BootstrapBaseController(BooksDbContext context, ISessionState sessionstate)
        {
            _context = context;
            _sessionstate = sessionstate;
        }

        protected BootstrapBaseController()
        {
            throw new System.NotImplementedException();
        }

        public void Attention(string message)
        {
            TempData.Add(Alerts.ATTENTION, message);
        }

        public void Success(string message)
        {
            TempData.Add(Alerts.SUCCESS, message);
        }

        public void Information(string message)
        {
            TempData.Add(Alerts.INFORMATION, message);
        }

        public void Error(string message)
        {
            TempData.Add(Alerts.ERROR, message);
        }
    }
}
