﻿

// This file was automatically generated.
// Do not make changes directly to this file - edit the template instead.
// 
// The following connection settings were used to generate this file
// 
//     Connection String Name: "BooksDbContext"
//     Connection String:      "Data Source=GAMER\SQLEXPRESS;Initial Catalog=Books;Integrated Security=True;Application Name=Books"

// ReSharper disable RedundantUsingDirective
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

//using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.DatabaseGeneratedOption;

namespace Books.DataAccess
{
    // ************************************************************************
    // Database context
    public class BooksDbContext : DbContext
    {
        public IDbSet<Acts> Acts { get; set; } // acts
        public IDbSet<Addresses> Addresses { get; set; } // addresses
        public IDbSet<Authors> Authors { get; set; } // authors
        public IDbSet<BibTypes> BibTypes { get; set; } // BibTypes
        public IDbSet<Checks> Checks { get; set; } // checks
        public IDbSet<Contracts> Contracts { get; set; } // contracts
        public IDbSet<Currencies> Currencies { get; set; } // Currencies
        public IDbSet<Formats> Formats { get; set; } // Formats
        public IDbSet<Invoices> Invoices { get; set; } // invoices
        public IDbSet<Journals> Journals { get; set; } // journals
        public IDbSet<OrderTypes> OrderTypes { get; set; } // OrderTypes
        public IDbSet<Payments> Payments { get; set; } // payments
        public IDbSet<Persons> Persons { get; set; } // persons
        public IDbSet<Publishers> Publishers { get; set; } // publishers
        public IDbSet<QArtOrder> QArtOrder { get; set; } // qArtOrder
        public IDbSet<QBookOrder> QBookOrder { get; set; } // qBookOrder
        public IDbSet<QInbookOrder> QInbookOrder { get; set; } // qInbookOrder
        public IDbSet<QIncollectionOrder> QIncollectionOrder { get; set; } // qIncollectionOrder
        public IDbSet<QryPagesByStatus> QryPagesByStatus { get; set; } // qryPagesByStatus
        public IDbSet<Sellers> Sellers { get; set; } // sellers
        public IDbSet<Statuses> Statuses { get; set; } // Statuses
        public IDbSet<Sysdiagrams> Sysdiagrams { get; set; } // sysdiagrams
        public IDbSet<TblSearchClean> TblSearchClean { get; set; } // tblSearchClean
        public IDbSet<TblSearchResults> TblSearchResults { get; set; } // tblSearchResults
        public IDbSet<Themes> Themes { get; set; } // themes
        public IDbSet<Users> Users { get; set; } // users
        public IDbSet<Works> Works { get; set; } // works
        public IDbSet<WorkTest> WorkTest { get; set; } // work_test

        static BooksDbContext()
        {
            Database.SetInitializer<BooksDbContext>(null);
        }

        public BooksDbContext()
            : base("Name=BooksDbContext")
        {
        }

        public BooksDbContext(string connectionString) : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ActsConfiguration());
            modelBuilder.Configurations.Add(new AddressesConfiguration());
            modelBuilder.Configurations.Add(new AuthorsConfiguration());
            modelBuilder.Configurations.Add(new BibTypesConfiguration());
            modelBuilder.Configurations.Add(new ChecksConfiguration());
            modelBuilder.Configurations.Add(new ContractsConfiguration());
            modelBuilder.Configurations.Add(new CurrenciesConfiguration());
            modelBuilder.Configurations.Add(new FormatsConfiguration());
            modelBuilder.Configurations.Add(new InvoicesConfiguration());
            modelBuilder.Configurations.Add(new JournalsConfiguration());
            modelBuilder.Configurations.Add(new OrderTypesConfiguration());
            modelBuilder.Configurations.Add(new PaymentsConfiguration());
            modelBuilder.Configurations.Add(new PersonsConfiguration());
            modelBuilder.Configurations.Add(new PublishersConfiguration());
            modelBuilder.Configurations.Add(new QArtOrderConfiguration());
            modelBuilder.Configurations.Add(new QBookOrderConfiguration());
            modelBuilder.Configurations.Add(new QInbookOrderConfiguration());
            modelBuilder.Configurations.Add(new QIncollectionOrderConfiguration());
            modelBuilder.Configurations.Add(new QryPagesByStatusConfiguration());
            modelBuilder.Configurations.Add(new SellersConfiguration());
            modelBuilder.Configurations.Add(new StatusesConfiguration());
            modelBuilder.Configurations.Add(new SysdiagramsConfiguration());
            modelBuilder.Configurations.Add(new TblSearchCleanConfiguration());
            modelBuilder.Configurations.Add(new TblSearchResultsConfiguration());
            modelBuilder.Configurations.Add(new ThemesConfiguration());
            modelBuilder.Configurations.Add(new UsersConfiguration());
            modelBuilder.Configurations.Add(new WorksConfiguration());
            modelBuilder.Configurations.Add(new WorkTestConfiguration());
        }
    }

    // ************************************************************************
    // POCO classes

    // acts
    public class Acts
    {
        public int Id { get; set; } // ID (Primary key)
        public int? ActNum { get; set; } // ActNum
        public int? Contract { get; set; } // Contract
        public DateTime? Date { get; set; } // Date
        public decimal? Value { get; set; } // Value
        public string Comm { get; set; } // Comm

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_act;

        public Acts()
        {
            Works = new List<Works>();
        }
    }

    // addresses
    public class Addresses
    {
        public int Код { get; set; } // Код (Primary key)
        public string City { get; set; } // City
        public string Country { get; set; } // Country
    }

    // authors
    public class Authors
    {
        public int Id { get; set; } // ID (Primary key)
        public int? PersonId { get; set; } // personID
        public int? Work { get; set; } // Work
        public string Role { get; set; } // Role
        public short? Index { get; set; } // index
        public string Comm { get; set; } // Comm
    }

    // BibTypes
    public class BibTypes
    {
        public string BibType { get; set; } // bibType (Primary key)
    }

    // checks
    public class Checks
    {
        public int Id { get; set; } // ID (Primary key)
        public string File { get; set; } // File
        public DateTime? Date { get; set; } // Date
        public int? Num { get; set; } // Num
        public string Agency { get; set; } // Agency
        public decimal? Value { get; set; } // Value
        public int? ExtractNum { get; set; } // ExtractNum
        public DateTime? DateWithdrawal { get; set; } // DateWithdrawal
        public string Payer { get; set; } // Payer
        public short? Offset { get; set; } // offset
        public DateTime? ReportedOn { get; set; } // ReportedOn

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_check;

        public Checks()
        {
            Works = new List<Works>();
        }
    }

    // contracts
    public class Contracts
    {
        public int Id { get; set; } // ID (Primary key)
        public DateTime? Date { get; set; } // Date
        public string Num { get; set; } // Num
        public string Partner { get; set; } // Partner
        public string Subject { get; set; } // Subject
        public decimal? Value { get; set; } // Value
        public DateTime? Expire { get; set; } // Expire
        public string Comm { get; set; } // Comm

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_contract;

        public Contracts()
        {
            Works = new List<Works>();
        }
    }

    // Currencies
    public class Currencies
    {
        public string Currency { get; set; } // Currency (Primary key)

        // Reverse navigation
        public virtual ICollection<Invoices> Invoices { get; set; } // invoices.FK_invoices_currency;
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_currency;

        public Currencies()
        {
            Invoices = new List<Invoices>();
            Works = new List<Works>();
        }
    }

    // Formats
    public class Formats
    {
        public string Format { get; set; } // Format (Primary key)

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_formats;

        public Formats()
        {
            Works = new List<Works>();
        }
    }

    // invoices
    public class Invoices
    {
        public int Id { get; set; } // ID (Primary key)
        public string File { get; set; } // File
        public string Seller { get; set; } // Seller
        public string Shop { get; set; } // Shop
        public string OrderNum { get; set; } // OrderNum
        public DateTime? Date { get; set; } // Date
        public decimal? TotalPrice { get; set; } // TotalPrice
        public string Currency { get; set; } // Currency
        public int? ExtractNum { get; set; } // ExtractNum
        public decimal? Eur { get; set; } // EUR
        public decimal? Rur { get; set; } // RUR
        public DateTime? DateWithdrawal { get; set; } // DateWithdrawal
        public DateTime? ReportedOn { get; set; } // ReportedOn
        public string RussianDesc { get; set; } // RussianDesc
        public int? AccountingId { get; set; } // AccountingID
        public short? NoInvoice { get; set; } // NoInvoice

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_invoice;

        // Foreign keys
        public virtual Currencies Currencies { get; set; } //  Currency - FK_invoices_currency

        public Invoices()
        {
            Works = new List<Works>();
        }
    }

    // journals
    public class Journals
    {
        public int Код { get; set; } // Код (Primary key)
        public string Name { get; set; } // Name
        public string Comm { get; set; } // Comm
    }

    // OrderTypes
    public class OrderTypes
    {
        public string OrderType { get; set; } // OrderType (Primary key)

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_ordertype;

        public OrderTypes()
        {
            Works = new List<Works>();
        }
    }

    // payments
    public class Payments
    {
        public short Id { get; set; } // ID (Primary key)
        public string Code { get; set; } // Code
        public string Explanation { get; set; } // Explanation
    }

    // persons
    public class Persons
    {
        public int Id { get; set; } // ID (Primary key)
        public string Name { get; set; } // Name
        public string Surname { get; set; } // Surname
        public string Comm { get; set; } // Comm
    }

    // publishers
    public class Publishers
    {
        public int Код { get; set; } // Код (Primary key)
        public string Name { get; set; } // Name
        public string Comm { get; set; } // Comm
    }

    // qArtOrder
    public class QArtOrder
    {
        public int Id { get; set; } // ID (Primary key)
        public string OrderType { get; set; } // OrderType
        public string Library { get; set; } // Library
        public string Format { get; set; } // Format
        public string BibType { get; set; } // bibType
        public string Author { get; set; } // author
        public string Title { get; set; } // title
        public string Journal { get; set; } // journal
        public string Year { get; set; } // year
        public string Number { get; set; } // number
        public string Pp { get; set; } // pp
        public DateTime? Ordered { get; set; } // Ordered
        public DateTime? Obtained { get; set; } // Obtained
        public DateTime? CreatedOn { get; set; } // CreatedOn
    }

    // qBookOrder
    public class QBookOrder
    {
        public int Id { get; set; } // ID (Primary key)
        public string OrderType { get; set; } // OrderType
        public string Format { get; set; } // Format
        public string BibType { get; set; } // bibType
        public string Author { get; set; } // author
        public string Editor { get; set; } // editor
        public string Title { get; set; } // title
        public string Publisher { get; set; } // publisher
        public string Year { get; set; } // year
        public string Volume { get; set; } // volume
        public string Pp { get; set; } // pp
        public string Series { get; set; } // series
        public string Address { get; set; } // address
        public string Isbn { get; set; } // ISBN
        public string Seller { get; set; } // seller
        public string Link { get; set; } // link
        public DateTime? Ordered { get; set; } // Ordered
        public DateTime? Obtained { get; set; } // Obtained
        public DateTime? CreatedOn { get; set; } // CreatedOn
        public DateTime? Scanned { get; set; } // Scanned
        public string WhereIs { get; set; } // WhereIs
        public string Comm { get; set; } // Comm
    }

    // qInbookOrder
    public class QInbookOrder
    {
        public int Id { get; set; } // ID (Primary key)
        public string OrderType { get; set; } // OrderType
        public string Library { get; set; } // Library
        public string Format { get; set; } // Format
        public string BibType { get; set; } // bibType
        public string Author { get; set; } // author
        public string Editor { get; set; } // editor
        public string Title { get; set; } // title
        public string Chapter { get; set; } // chapter
        public string Pp { get; set; } // pp
        public string Publisher { get; set; } // publisher
        public string Year { get; set; } // year
        public string Volume { get; set; } // volume
        public string Number { get; set; } // number
        public string Series { get; set; } // series
        public string Address { get; set; } // address
        public string Isbn { get; set; } // ISBN
        public DateTime? CreatedOn { get; set; } // CreatedOn
        public DateTime? Ordered { get; set; } // Ordered
        public DateTime? Obtained { get; set; } // Obtained
    }

    // qIncollectionOrder
    public class QIncollectionOrder
    {
        public DateTime? CreatedOn { get; set; } // CreatedOn
        public int Id { get; set; } // ID (Primary key)
        public string OrderType { get; set; } // OrderType
        public string Library { get; set; } // Library
        public string Format { get; set; } // Format
        public string BibType { get; set; } // bibType
        public string Author { get; set; } // author
        public string Title { get; set; } // title
        public string Booktitle { get; set; } // booktitle
        public string Publisher { get; set; } // publisher
        public string Year { get; set; } // year
        public string Editor { get; set; } // editor
        public string Volume { get; set; } // volume
        public string Series { get; set; } // series
        public string Chapter { get; set; } // chapter
        public string Pp { get; set; } // pp
        public string Address { get; set; } // address
    }

    // qryPagesByStatus
    public class QryPagesByStatus
    {
        public double? SumOfcount { get; set; } // SumOfcount
        public string Status { get; set; } // Status (Primary key)
    }

    // sellers
    public class Sellers
    {
        public int Id { get; set; } // ID (Primary key)
        public string Name { get; set; } // Name
        public string Site { get; set; } // site
        public string EMail { get; set; } // e-mail
        public string Comm { get; set; } // Comm
    }

    // Statuses
    public class Statuses
    {
        public string Status { get; set; } // Status (Primary key)

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_status;

        public Statuses()
        {
            Works = new List<Works>();
        }
    }

    // sysdiagrams
    public class Sysdiagrams
    {
        public string Name { get; set; } // name
        public int PrincipalId { get; set; } // principal_id
        public int DiagramId { get; set; } // diagram_id (Primary key)
        public int? Version { get; set; } // version
        public string Definition { get; set; } // definition
    }

    // tblSearchClean
    public class TblSearchClean
    {
        public int Id { get; set; } // ID (Primary key)
        public string BibType { get; set; } // bibType
        public string Type { get; set; } // type
        public string Language { get; set; } // language
        public string Author { get; set; } // author
        public string Editor { get; set; } // editor
        public string Title { get; set; } // title
        public string Sourcetitle { get; set; } // sourcetitle
        public string Publisher { get; set; } // publisher
        public string Year { get; set; } // year
        public string Volume { get; set; } // volume
        public string Number { get; set; } // number
        public string Pp { get; set; } // pp
        public string Series { get; set; } // series
        public string Address { get; set; } // address
        public string Isbn { get; set; } // ISBN
        public string Issn { get; set; } // ISSN
        public int? Crossref { get; set; } // crossref
        public string Url { get; set; } // url
        public string Institution { get; set; } // institution
        public string File { get; set; } // File
        public string Note { get; set; } // note
        public string Edition { get; set; } // edition
        public string Size { get; set; } // size
        public string Bibliography { get; set; } // bibliography
        public string Keywords { get; set; } // keywords
        public string Region { get; set; } // region
        public string Topic { get; set; } // topic
        public string Journal { get; set; } // journal
        public string Booktitle { get; set; } // booktitle
        public string Persons { get; set; } // persons
        public string MarcSource { get; set; } // MARCSource
        public string MarcRecord { get; set; } // MARCRecord
    }

    // tblSearchResults
    public class TblSearchResults
    {
        public int Id { get; set; } // ID (Primary key)
        public string BibType { get; set; } // bibType
        public string Type { get; set; } // type
        public string Language { get; set; } // language
        public string Author { get; set; } // author
        public string Editor { get; set; } // editor
        public string Title { get; set; } // title
        public string Sourcetitle { get; set; } // sourcetitle
        public string Publisher { get; set; } // publisher
        public string Year { get; set; } // year
        public string Volume { get; set; } // volume
        public string Number { get; set; } // number
        public string Pp { get; set; } // pp
        public string Series { get; set; } // series
        public string Address { get; set; } // address
        public string Isbn { get; set; } // ISBN
        public string Issn { get; set; } // ISSN
        public int? Crossref { get; set; } // crossref
        public string Url { get; set; } // url
        public string Institution { get; set; } // institution
        public string File { get; set; } // File
        public string Note { get; set; } // note
        public string Edition { get; set; } // edition
        public string Size { get; set; } // size
        public string Bibliography { get; set; } // bibliography
        public string Keywords { get; set; } // keywords
        public string Region { get; set; } // region
        public string Topic { get; set; } // topic
        public string Journal { get; set; } // journal
        public string Booktitle { get; set; } // booktitle
        public string Persons { get; set; } // persons
        public string MarcSource { get; set; } // MARCSource
        public string MarcRecord { get; set; } // MARCRecord
    }

    // themes
    public class Themes
    {
        public int Id { get; set; } // ID (Primary key)
        public string Title { get; set; } // Title
    }

    // users
    public class Users
    {
        public short Id { get; set; } // ID (Primary key)
        public string Name { get; set; } // Name
    }

    // works
    public class Works
    {
        public int Id { get; set; } // ID (Primary key)
        public DateTime? CreatedOn { get; set; } // CreatedOn
        public string Status { get; set; } // Status
        public string User { get; set; } // user
        public string Theme { get; set; } // theme
        public string OrderType { get; set; } // OrderType
        public string Library { get; set; } // Library
        public string Format { get; set; } // Format
        public string BibType { get; set; } // bibType
        public string Author { get; set; } // author
        public string Editor { get; set; } // editor
        public string Title { get; set; } // title
        public string Booktitle { get; set; } // booktitle
        public string Publisher { get; set; } // publisher
        public string Journal { get; set; } // journal
        public string Year { get; set; } // year
        public string Volume { get; set; } // volume
        public string Number { get; set; } // number
        public string Chapter { get; set; } // chapter
        public string Pp { get; set; } // pp
        public string Series { get; set; } // series
        public string Address { get; set; } // address
        public string Isbn { get; set; } // ISBN
        public int? Source { get; set; } // source
        public string Seller { get; set; } // seller
        public string Link { get; set; } // link
        public decimal? PrimaryPrice { get; set; } // PrimaryPrice
        public string Currency { get; set; } // Currency
        public DateTime? Ordered { get; set; } // Ordered
        public string OrderId { get; set; } // OrderID
        public string PostId { get; set; } // PostID
        public DateTime? Obtained { get; set; } // Obtained
        public string History { get; set; } // history
        public int? Invoice { get; set; } // invoice
        public int? Contract { get; set; } // contract
        public int? Check { get; set; } // check
        public string CopyRange { get; set; } // copyRange
        public int? Count { get; set; } // count
        public string Size { get; set; } // size
        public DateTime? Scanning { get; set; } // Scanning
        public DateTime? Scanned { get; set; } // Scanned
        public int? Act { get; set; } // Act
        public string File { get; set; } // File
        public string Path { get; set; } // Path
        public string WhereIs { get; set; } // WhereIs
        public string Comm { get; set; } // Comm

        // Reverse navigation
        public virtual ICollection<Works> Works2 { get; set; } // works.FK_works_works;

        // Foreign keys
        public virtual Statuses Statuses { get; set; } //  Status - FK_works_status
        public virtual OrderTypes OrderTypes { get; set; } //  OrderType - FK_works_ordertype
        public virtual Formats Formats { get; set; } //  Format - FK_works_formats
        public virtual Works Works1 { get; set; } //  Source - FK_works_works
        public virtual Currencies Currencies { get; set; } //  Currency - FK_works_currency
        public virtual Invoices Invoices { get; set; } //  Invoice - FK_works_invoice
        public virtual Contracts Contracts { get; set; } //  Contract - FK_works_contract
        public virtual Checks Checks { get; set; } //  Check - FK_works_check
        public virtual Acts Acts { get; set; } //  Act - FK_works_act

        public Works()
        {
            Works2 = new List<Works>();
        }
    }

    // work_test
    public class WorkTest
    {
        public int? IDauto { get; set; } // IDauto
        public int Id { get; set; } // ID (Primary key)
        public DateTime? CreatedOn { get; set; } // CreatedOn
        public string User { get; set; } // user
        public string Theme { get; set; } // theme
        public string OrderType { get; set; } // OrderType
        public string Library { get; set; } // Library
        public string Format { get; set; } // Format
        public string BibType { get; set; } // bibType
        public string Author { get; set; } // author
        public string Editor { get; set; } // editor
        public string Title { get; set; } // title
        public string Booktitle { get; set; } // booktitle
        public string Publisher { get; set; } // publisher
        public string Journal { get; set; } // journal
        public string Year { get; set; } // year
        public string Volume { get; set; } // volume
        public string Number { get; set; } // number
        public string Chapter { get; set; } // chapter
        public string Pp { get; set; } // pp
        public string Series { get; set; } // series
        public string Address { get; set; } // address
        public string Isbn { get; set; } // ISBN
        public int? Source { get; set; } // source
        public string Seller { get; set; } // seller
        public string Link { get; set; } // link
        public decimal? PrimaryPrice { get; set; } // PrimaryPrice
        public string Currency { get; set; } // Currency
        public DateTime? Ordered { get; set; } // Ordered
        public string OrderId { get; set; } // OrderID
        public string PostId { get; set; } // PostID
        public DateTime? Obtained { get; set; } // Obtained
        public string History { get; set; } // history
        public int? Invoice { get; set; } // invoice
        public int? Contract { get; set; } // contract
        public int? Check { get; set; } // check
        public DateTime? ReportedOn { get; set; } // ReportedOn
        public string CopyRange { get; set; } // copyRange
        public int? Count { get; set; } // count
        public string Size { get; set; } // size
        public DateTime? Scanning { get; set; } // Scanning
        public DateTime? Scanned { get; set; } // Scanned
        public int? Act { get; set; } // Act
        public string File { get; set; } // File
        public string Path { get; set; } // Path
        public string WhereIs { get; set; } // WhereIs
        public short? Completed { get; set; } // Completed
        public string Comm { get; set; } // Comm
    }


    // ************************************************************************
    // POCO Configuration

    // acts
    internal class ActsConfiguration : EntityTypeConfiguration<Acts>
    {
        public ActsConfiguration()
        {
            ToTable("dbo.acts");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ActNum).HasColumnName("ActNum").IsOptional();
            Property(x => x.Contract).HasColumnName("Contract").IsOptional();
            Property(x => x.Date).HasColumnName("Date").IsOptional();
            Property(x => x.Value).HasColumnName("Value").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }

    // addresses
    internal class AddressesConfiguration : EntityTypeConfiguration<Addresses>
    {
        public AddressesConfiguration()
        {
            ToTable("dbo.addresses");
            HasKey(x => x.Код);

            Property(x => x.Код).HasColumnName("Код").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.City).HasColumnName("City").IsOptional();
            Property(x => x.Country).HasColumnName("Country").IsOptional();
        }
    }

    // authors
    internal class AuthorsConfiguration : EntityTypeConfiguration<Authors>
    {
        public AuthorsConfiguration()
        {
            ToTable("dbo.authors");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("personID").IsOptional();
            Property(x => x.Work).HasColumnName("Work").IsOptional();
            Property(x => x.Role).HasColumnName("Role").IsOptional().HasMaxLength(100);
            Property(x => x.Index).HasColumnName("index").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional();
        }
    }

    // BibTypes
    internal class BibTypesConfiguration : EntityTypeConfiguration<BibTypes>
    {
        public BibTypesConfiguration()
        {
            ToTable("dbo.BibTypes");
            HasKey(x => x.BibType);

            Property(x => x.BibType).HasColumnName("bibType").IsRequired().HasMaxLength(12);
        }
    }

    // checks
    internal class ChecksConfiguration : EntityTypeConfiguration<Checks>
    {
        public ChecksConfiguration()
        {
            ToTable("dbo.checks");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Date).HasColumnName("Date").IsOptional();
            Property(x => x.Num).HasColumnName("Num").IsOptional();
            Property(x => x.Agency).HasColumnName("Agency").IsOptional().HasMaxLength(200);
            Property(x => x.Value).HasColumnName("Value").IsOptional();
            Property(x => x.ExtractNum).HasColumnName("ExtractNum").IsOptional();
            Property(x => x.DateWithdrawal).HasColumnName("DateWithdrawal").IsOptional();
            Property(x => x.Payer).HasColumnName("Payer").IsOptional().HasMaxLength(90);
            Property(x => x.Offset).HasColumnName("offset").IsOptional();
            Property(x => x.ReportedOn).HasColumnName("ReportedOn").IsOptional();
        }
    }

    // contracts
    internal class ContractsConfiguration : EntityTypeConfiguration<Contracts>
    {
        public ContractsConfiguration()
        {
            ToTable("dbo.contracts");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Date).HasColumnName("Date").IsOptional();
            Property(x => x.Num).HasColumnName("Num").IsOptional().HasMaxLength(90);
            Property(x => x.Partner).HasColumnName("Partner").IsOptional();
            Property(x => x.Subject).HasColumnName("Subject").IsOptional();
            Property(x => x.Value).HasColumnName("Value").IsOptional();
            Property(x => x.Expire).HasColumnName("Expire").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }

    // Currencies
    internal class CurrenciesConfiguration : EntityTypeConfiguration<Currencies>
    {
        public CurrenciesConfiguration()
        {
            ToTable("dbo.Currencies");
            HasKey(x => x.Currency);

            Property(x => x.Currency).HasColumnName("Currency").IsRequired().HasMaxLength(3);
        }
    }

    // Formats
    internal class FormatsConfiguration : EntityTypeConfiguration<Formats>
    {
        public FormatsConfiguration()
        {
            ToTable("dbo.Formats");
            HasKey(x => x.Format);

            Property(x => x.Format).HasColumnName("Format").IsRequired().HasMaxLength(10);
        }
    }

    // invoices
    internal class InvoicesConfiguration : EntityTypeConfiguration<Invoices>
    {
        public InvoicesConfiguration()
        {
            ToTable("dbo.invoices");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Seller).HasColumnName("Seller").IsOptional().HasMaxLength(100);
            Property(x => x.Shop).HasColumnName("Shop").IsOptional().HasMaxLength(45);
            Property(x => x.OrderNum).HasColumnName("OrderNum").IsOptional().HasMaxLength(45);
            Property(x => x.Date).HasColumnName("Date").IsOptional();
            Property(x => x.TotalPrice).HasColumnName("TotalPrice").IsOptional();
            Property(x => x.Currency).HasColumnName("Currency").IsOptional().HasMaxLength(3);
            Property(x => x.ExtractNum).HasColumnName("ExtractNum").IsOptional();
            Property(x => x.Eur).HasColumnName("EUR").IsOptional();
            Property(x => x.Rur).HasColumnName("RUR").IsOptional();
            Property(x => x.DateWithdrawal).HasColumnName("DateWithdrawal").IsOptional();
            Property(x => x.ReportedOn).HasColumnName("ReportedOn").IsOptional();
            Property(x => x.RussianDesc).HasColumnName("RussianDesc").IsOptional();
            Property(x => x.AccountingId).HasColumnName("AccountingID").IsOptional();
            Property(x => x.NoInvoice).HasColumnName("NoInvoice").IsOptional();

            // Foreign keys
            HasOptional(a => a.Currencies).WithMany(b => b.Invoices).HasForeignKey(c => c.Currency); // FK_invoices_currency
        }
    }

    // journals
    internal class JournalsConfiguration : EntityTypeConfiguration<Journals>
    {
        public JournalsConfiguration()
        {
            ToTable("dbo.journals");
            HasKey(x => x.Код);

            Property(x => x.Код).HasColumnName("Код").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional();
        }
    }

    // OrderTypes
    internal class OrderTypesConfiguration : EntityTypeConfiguration<OrderTypes>
    {
        public OrderTypesConfiguration()
        {
            ToTable("dbo.OrderTypes");
            HasKey(x => x.OrderType);

            Property(x => x.OrderType).HasColumnName("OrderType").IsRequired().HasMaxLength(11);
        }
    }

    // payments
    internal class PaymentsConfiguration : EntityTypeConfiguration<Payments>
    {
        public PaymentsConfiguration()
        {
            ToTable("dbo.payments");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Code).HasColumnName("Code").IsOptional().HasMaxLength(100);
            Property(x => x.Explanation).HasColumnName("Explanation").IsOptional().HasMaxLength(100);
        }
    }

    // persons
    internal class PersonsConfiguration : EntityTypeConfiguration<Persons>
    {
        public PersonsConfiguration()
        {
            ToTable("dbo.persons");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(255);
            Property(x => x.Surname).HasColumnName("Surname").IsOptional().HasMaxLength(255);
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }

    // publishers
    internal class PublishersConfiguration : EntityTypeConfiguration<Publishers>
    {
        public PublishersConfiguration()
        {
            ToTable("dbo.publishers");
            HasKey(x => x.Код);

            Property(x => x.Код).HasColumnName("Код").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(255);
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }

    // qArtOrder
    internal class QArtOrderConfiguration : EntityTypeConfiguration<QArtOrder>
    {
        public QArtOrderConfiguration()
        {
            ToTable("dbo.qArtOrder");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.OrderType).HasColumnName("OrderType").IsOptional().HasMaxLength(11);
            Property(x => x.Library).HasColumnName("Library").IsOptional().HasMaxLength(6);
            Property(x => x.Format).HasColumnName("Format").IsOptional().HasMaxLength(10);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Journal).HasColumnName("journal").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(50);
            Property(x => x.Number).HasColumnName("number").IsOptional().HasMaxLength(50);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Ordered).HasColumnName("Ordered").IsOptional();
            Property(x => x.Obtained).HasColumnName("Obtained").IsOptional();
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
        }
    }

    // qBookOrder
    internal class QBookOrderConfiguration : EntityTypeConfiguration<QBookOrder>
    {
        public QBookOrderConfiguration()
        {
            ToTable("dbo.qBookOrder");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.OrderType).HasColumnName("OrderType").IsOptional().HasMaxLength(11);
            Property(x => x.Format).HasColumnName("Format").IsOptional().HasMaxLength(10);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Editor).HasColumnName("editor").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Publisher).HasColumnName("publisher").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(50);
            Property(x => x.Volume).HasColumnName("volume").IsOptional().HasMaxLength(50);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Series).HasColumnName("series").IsOptional().HasMaxLength(255);
            Property(x => x.Address).HasColumnName("address").IsOptional().HasMaxLength(100);
            Property(x => x.Isbn).HasColumnName("ISBN").IsOptional().HasMaxLength(100);
            Property(x => x.Seller).HasColumnName("seller").IsOptional().HasMaxLength(255);
            Property(x => x.Link).HasColumnName("link").IsOptional().HasMaxLength(255);
            Property(x => x.Ordered).HasColumnName("Ordered").IsOptional();
            Property(x => x.Obtained).HasColumnName("Obtained").IsOptional();
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.Scanned).HasColumnName("Scanned").IsOptional();
            Property(x => x.WhereIs).HasColumnName("WhereIs").IsOptional().HasMaxLength(100);
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }

    // qInbookOrder
    internal class QInbookOrderConfiguration : EntityTypeConfiguration<QInbookOrder>
    {
        public QInbookOrderConfiguration()
        {
            ToTable("dbo.qInbookOrder");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.OrderType).HasColumnName("OrderType").IsOptional().HasMaxLength(11);
            Property(x => x.Library).HasColumnName("Library").IsOptional().HasMaxLength(6);
            Property(x => x.Format).HasColumnName("Format").IsOptional().HasMaxLength(10);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Editor).HasColumnName("editor").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Chapter).HasColumnName("chapter").IsOptional().HasMaxLength(255);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Publisher).HasColumnName("publisher").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(50);
            Property(x => x.Volume).HasColumnName("volume").IsOptional().HasMaxLength(50);
            Property(x => x.Number).HasColumnName("number").IsOptional().HasMaxLength(50);
            Property(x => x.Series).HasColumnName("series").IsOptional().HasMaxLength(255);
            Property(x => x.Address).HasColumnName("address").IsOptional().HasMaxLength(100);
            Property(x => x.Isbn).HasColumnName("ISBN").IsOptional().HasMaxLength(100);
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.Ordered).HasColumnName("Ordered").IsOptional();
            Property(x => x.Obtained).HasColumnName("Obtained").IsOptional();
        }
    }

    // qIncollectionOrder
    internal class QIncollectionOrderConfiguration : EntityTypeConfiguration<QIncollectionOrder>
    {
        public QIncollectionOrderConfiguration()
        {
            ToTable("dbo.qIncollectionOrder");
            HasKey(x => x.Id);

            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.OrderType).HasColumnName("OrderType").IsOptional().HasMaxLength(11);
            Property(x => x.Library).HasColumnName("Library").IsOptional().HasMaxLength(6);
            Property(x => x.Format).HasColumnName("Format").IsOptional().HasMaxLength(10);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Booktitle).HasColumnName("booktitle").IsOptional().HasMaxLength(255);
            Property(x => x.Publisher).HasColumnName("publisher").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(50);
            Property(x => x.Editor).HasColumnName("editor").IsOptional().HasMaxLength(255);
            Property(x => x.Volume).HasColumnName("volume").IsOptional().HasMaxLength(50);
            Property(x => x.Series).HasColumnName("series").IsOptional().HasMaxLength(255);
            Property(x => x.Chapter).HasColumnName("chapter").IsOptional().HasMaxLength(255);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Address).HasColumnName("address").IsOptional().HasMaxLength(100);
        }
    }

    // qryPagesByStatus
    internal class QryPagesByStatusConfiguration : EntityTypeConfiguration<QryPagesByStatus>
    {
        public QryPagesByStatusConfiguration()
        {
            ToTable("dbo.qryPagesByStatus");
            HasKey(x => x.Status);

            Property(x => x.SumOfcount).HasColumnName("SumOfcount").IsOptional();
            Property(x => x.Status).HasColumnName("Status").IsRequired().HasMaxLength(35);
        }
    }

    // sellers
    internal class SellersConfiguration : EntityTypeConfiguration<Sellers>
    {
        public SellersConfiguration()
        {
            ToTable("dbo.sellers");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(255);
            Property(x => x.Site).HasColumnName("site").IsOptional().HasMaxLength(255);
            Property(x => x.EMail).HasColumnName("e_mail").IsOptional().HasMaxLength(255);
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }

    // Statuses
    internal class StatusesConfiguration : EntityTypeConfiguration<Statuses>
    {
        public StatusesConfiguration()
        {
            ToTable("dbo.Statuses");
            HasKey(x => x.Status);

            Property(x => x.Status).HasColumnName("Status").IsRequired().HasMaxLength(35);
        }
    }

    // sysdiagrams
    internal class SysdiagramsConfiguration : EntityTypeConfiguration<Sysdiagrams>
    {
        public SysdiagramsConfiguration()
        {
            ToTable("dbo.sysdiagrams");
            HasKey(x => x.DiagramId);

            Property(x => x.Name).HasColumnName("name").IsRequired().HasMaxLength(128);
            Property(x => x.PrincipalId).HasColumnName("principal_id").IsRequired();
            Property(x => x.DiagramId).HasColumnName("diagram_id").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Version).HasColumnName("version").IsOptional();
            Property(x => x.Definition).HasColumnName("definition").IsOptional();
        }
    }

    // tblSearchClean
    internal class TblSearchCleanConfiguration : EntityTypeConfiguration<TblSearchClean>
    {
        public TblSearchCleanConfiguration()
        {
            ToTable("dbo.tblSearchClean");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Type).HasColumnName("type").IsOptional().HasMaxLength(255);
            Property(x => x.Language).HasColumnName("language").IsOptional().HasMaxLength(255);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Editor).HasColumnName("editor").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Sourcetitle).HasColumnName("sourcetitle").IsOptional().HasMaxLength(255);
            Property(x => x.Publisher).HasColumnName("publisher").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(255);
            Property(x => x.Volume).HasColumnName("volume").IsOptional().HasMaxLength(255);
            Property(x => x.Number).HasColumnName("number").IsOptional().HasMaxLength(255);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Series).HasColumnName("series").IsOptional().HasMaxLength(255);
            Property(x => x.Address).HasColumnName("address").IsOptional().HasMaxLength(255);
            Property(x => x.Isbn).HasColumnName("ISBN").IsOptional().HasMaxLength(255);
            Property(x => x.Issn).HasColumnName("ISSN").IsOptional().HasMaxLength(255);
            Property(x => x.Crossref).HasColumnName("crossref").IsOptional();
            Property(x => x.Url).HasColumnName("url").IsOptional().HasMaxLength(255);
            Property(x => x.Institution).HasColumnName("institution").IsOptional().HasMaxLength(255);
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Note).HasColumnName("note").IsOptional();
            Property(x => x.Edition).HasColumnName("edition").IsOptional().HasMaxLength(255);
            Property(x => x.Size).HasColumnName("size").IsOptional().HasMaxLength(255);
            Property(x => x.Bibliography).HasColumnName("bibliography").IsOptional().HasMaxLength(255);
            Property(x => x.Keywords).HasColumnName("keywords").IsOptional();
            Property(x => x.Region).HasColumnName("region").IsOptional().HasMaxLength(255);
            Property(x => x.Topic).HasColumnName("topic").IsOptional().HasMaxLength(255);
            Property(x => x.Journal).HasColumnName("journal").IsOptional().HasMaxLength(255);
            Property(x => x.Booktitle).HasColumnName("booktitle").IsOptional().HasMaxLength(255);
            Property(x => x.Persons).HasColumnName("persons").IsOptional();
            Property(x => x.MarcSource).HasColumnName("MARCSource").IsOptional().HasMaxLength(255);
            Property(x => x.MarcRecord).HasColumnName("MARCRecord").IsOptional();
        }
    }

    // tblSearchResults
    internal class TblSearchResultsConfiguration : EntityTypeConfiguration<TblSearchResults>
    {
        public TblSearchResultsConfiguration()
        {
            ToTable("dbo.tblSearchResults");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Type).HasColumnName("type").IsOptional().HasMaxLength(255);
            Property(x => x.Language).HasColumnName("language").IsOptional().HasMaxLength(255);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Editor).HasColumnName("editor").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Sourcetitle).HasColumnName("sourcetitle").IsOptional().HasMaxLength(255);
            Property(x => x.Publisher).HasColumnName("publisher").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(255);
            Property(x => x.Volume).HasColumnName("volume").IsOptional().HasMaxLength(255);
            Property(x => x.Number).HasColumnName("number").IsOptional().HasMaxLength(255);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Series).HasColumnName("series").IsOptional().HasMaxLength(255);
            Property(x => x.Address).HasColumnName("address").IsOptional().HasMaxLength(255);
            Property(x => x.Isbn).HasColumnName("ISBN").IsOptional().HasMaxLength(255);
            Property(x => x.Issn).HasColumnName("ISSN").IsOptional().HasMaxLength(255);
            Property(x => x.Crossref).HasColumnName("crossref").IsOptional();
            Property(x => x.Url).HasColumnName("url").IsOptional().HasMaxLength(255);
            Property(x => x.Institution).HasColumnName("institution").IsOptional().HasMaxLength(255);
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Note).HasColumnName("note").IsOptional();
            Property(x => x.Edition).HasColumnName("edition").IsOptional().HasMaxLength(255);
            Property(x => x.Size).HasColumnName("size").IsOptional().HasMaxLength(255);
            Property(x => x.Bibliography).HasColumnName("bibliography").IsOptional().HasMaxLength(255);
            Property(x => x.Keywords).HasColumnName("keywords").IsOptional();
            Property(x => x.Region).HasColumnName("region").IsOptional().HasMaxLength(255);
            Property(x => x.Topic).HasColumnName("topic").IsOptional().HasMaxLength(255);
            Property(x => x.Journal).HasColumnName("journal").IsOptional().HasMaxLength(255);
            Property(x => x.Booktitle).HasColumnName("booktitle").IsOptional().HasMaxLength(255);
            Property(x => x.Persons).HasColumnName("persons").IsOptional();
            Property(x => x.MarcSource).HasColumnName("MARCSource").IsOptional().HasMaxLength(255);
            Property(x => x.MarcRecord).HasColumnName("MARCRecord").IsOptional();
        }
    }

    // themes
    internal class ThemesConfiguration : EntityTypeConfiguration<Themes>
    {
        public ThemesConfiguration()
        {
            ToTable("dbo.themes");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Title).HasColumnName("Title").IsOptional().HasMaxLength(90);
        }
    }

    // users
    internal class UsersConfiguration : EntityTypeConfiguration<Users>
    {
        public UsersConfiguration()
        {
            ToTable("dbo.users");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(90);
        }
    }

    // works
    internal class WorksConfiguration : EntityTypeConfiguration<Works>
    {
        public WorksConfiguration()
        {
            ToTable("dbo.works");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.Status).HasColumnName("Status").IsOptional().HasMaxLength(35);
            Property(x => x.User).HasColumnName("user").IsOptional().HasMaxLength(100);
            Property(x => x.Theme).HasColumnName("theme").IsOptional().HasMaxLength(100);
            Property(x => x.OrderType).HasColumnName("OrderType").IsOptional().HasMaxLength(11);
            Property(x => x.Library).HasColumnName("Library").IsOptional().HasMaxLength(6);
            Property(x => x.Format).HasColumnName("Format").IsOptional().HasMaxLength(10);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Editor).HasColumnName("editor").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Booktitle).HasColumnName("booktitle").IsOptional().HasMaxLength(255);
            Property(x => x.Publisher).HasColumnName("publisher").IsOptional().HasMaxLength(255);
            Property(x => x.Journal).HasColumnName("journal").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(50);
            Property(x => x.Volume).HasColumnName("volume").IsOptional().HasMaxLength(50);
            Property(x => x.Number).HasColumnName("number").IsOptional().HasMaxLength(50);
            Property(x => x.Chapter).HasColumnName("chapter").IsOptional().HasMaxLength(255);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Series).HasColumnName("series").IsOptional().HasMaxLength(255);
            Property(x => x.Address).HasColumnName("address").IsOptional().HasMaxLength(100);
            Property(x => x.Isbn).HasColumnName("ISBN").IsOptional().HasMaxLength(100);
            Property(x => x.Source).HasColumnName("source").IsOptional();
            Property(x => x.Seller).HasColumnName("seller").IsOptional().HasMaxLength(255);
            Property(x => x.Link).HasColumnName("link").IsOptional().HasMaxLength(255);
            Property(x => x.PrimaryPrice).HasColumnName("PrimaryPrice").IsOptional();
            Property(x => x.Currency).HasColumnName("Currency").IsOptional().HasMaxLength(3);
            Property(x => x.Ordered).HasColumnName("Ordered").IsOptional();
            Property(x => x.OrderId).HasColumnName("OrderID").IsOptional().HasMaxLength(255);
            Property(x => x.PostId).HasColumnName("PostID").IsOptional().HasMaxLength(255);
            Property(x => x.Obtained).HasColumnName("Obtained").IsOptional();
            Property(x => x.History).HasColumnName("history").IsOptional().HasMaxLength(255);
            Property(x => x.Invoice).HasColumnName("invoice").IsOptional();
            Property(x => x.Contract).HasColumnName("contract").IsOptional();
            Property(x => x.Check).HasColumnName("check").IsOptional();
            Property(x => x.CopyRange).HasColumnName("copyRange").IsOptional().HasMaxLength(255);
            Property(x => x.Count).HasColumnName("count").IsOptional();
            Property(x => x.Size).HasColumnName("size").IsOptional().HasMaxLength(2);
            Property(x => x.Scanning).HasColumnName("Scanning").IsOptional();
            Property(x => x.Scanned).HasColumnName("Scanned").IsOptional();
            Property(x => x.Act).HasColumnName("Act").IsOptional();
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Path).HasColumnName("Path").IsOptional().HasMaxLength(255);
            Property(x => x.WhereIs).HasColumnName("WhereIs").IsOptional().HasMaxLength(100);
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);

            // Foreign keys
            HasOptional(a => a.Statuses).WithMany(b => b.Works).HasForeignKey(c => c.Status); // FK_works_status
            HasOptional(a => a.OrderTypes).WithMany(b => b.Works).HasForeignKey(c => c.OrderType); // FK_works_ordertype
            HasOptional(a => a.Formats).WithMany(b => b.Works).HasForeignKey(c => c.Format); // FK_works_formats
            HasOptional(a => a.Works1).WithMany(b => b.Works2).HasForeignKey(c => c.Source); // FK_works_works
            HasOptional(a => a.Currencies).WithMany(b => b.Works).HasForeignKey(c => c.Currency); // FK_works_currency
            HasOptional(a => a.Invoices).WithMany(b => b.Works).HasForeignKey(c => c.Invoice); // FK_works_invoice
            HasOptional(a => a.Contracts).WithMany(b => b.Works).HasForeignKey(c => c.Contract); // FK_works_contract
            HasOptional(a => a.Checks).WithMany(b => b.Works).HasForeignKey(c => c.Check); // FK_works_check
            HasOptional(a => a.Acts).WithMany(b => b.Works).HasForeignKey(c => c.Act); // FK_works_act
        }
    }

    // work_test
    internal class WorkTestConfiguration : EntityTypeConfiguration<WorkTest>
    {
        public WorkTestConfiguration()
        {
            ToTable("dbo.work_test");
            HasKey(x => x.Id);

            Property(x => x.IDauto).HasColumnName("IDauto").IsOptional();
            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.User).HasColumnName("user").IsOptional().HasMaxLength(100);
            Property(x => x.Theme).HasColumnName("theme").IsOptional().HasMaxLength(100);
            Property(x => x.OrderType).HasColumnName("OrderType").IsOptional().HasMaxLength(11);
            Property(x => x.Library).HasColumnName("Library").IsOptional().HasMaxLength(6);
            Property(x => x.Format).HasColumnName("Format").IsOptional().HasMaxLength(10);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Editor).HasColumnName("editor").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Booktitle).HasColumnName("booktitle").IsOptional().HasMaxLength(255);
            Property(x => x.Publisher).HasColumnName("publisher").IsOptional().HasMaxLength(255);
            Property(x => x.Journal).HasColumnName("journal").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(50);
            Property(x => x.Volume).HasColumnName("volume").IsOptional().HasMaxLength(50);
            Property(x => x.Number).HasColumnName("number").IsOptional().HasMaxLength(50);
            Property(x => x.Chapter).HasColumnName("chapter").IsOptional().HasMaxLength(255);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Series).HasColumnName("series").IsOptional().HasMaxLength(255);
            Property(x => x.Address).HasColumnName("address").IsOptional().HasMaxLength(100);
            Property(x => x.Isbn).HasColumnName("ISBN").IsOptional().HasMaxLength(100);
            Property(x => x.Source).HasColumnName("source").IsOptional();
            Property(x => x.Seller).HasColumnName("seller").IsOptional().HasMaxLength(255);
            Property(x => x.Link).HasColumnName("link").IsOptional().HasMaxLength(255);
            Property(x => x.PrimaryPrice).HasColumnName("PrimaryPrice").IsOptional();
            Property(x => x.Currency).HasColumnName("Currency").IsOptional().HasMaxLength(3);
            Property(x => x.Ordered).HasColumnName("Ordered").IsOptional();
            Property(x => x.OrderId).HasColumnName("OrderID").IsOptional().HasMaxLength(255);
            Property(x => x.PostId).HasColumnName("PostID").IsOptional().HasMaxLength(255);
            Property(x => x.Obtained).HasColumnName("Obtained").IsOptional();
            Property(x => x.History).HasColumnName("history").IsOptional().HasMaxLength(255);
            Property(x => x.Invoice).HasColumnName("invoice").IsOptional();
            Property(x => x.Contract).HasColumnName("contract").IsOptional();
            Property(x => x.Check).HasColumnName("check").IsOptional();
            Property(x => x.ReportedOn).HasColumnName("ReportedOn").IsOptional();
            Property(x => x.CopyRange).HasColumnName("copyRange").IsOptional().HasMaxLength(255);
            Property(x => x.Count).HasColumnName("count").IsOptional();
            Property(x => x.Size).HasColumnName("size").IsOptional().HasMaxLength(2);
            Property(x => x.Scanning).HasColumnName("Scanning").IsOptional();
            Property(x => x.Scanned).HasColumnName("Scanned").IsOptional();
            Property(x => x.Act).HasColumnName("Act").IsOptional();
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Path).HasColumnName("Path").IsOptional().HasMaxLength(255);
            Property(x => x.WhereIs).HasColumnName("WhereIs").IsOptional().HasMaxLength(100);
            Property(x => x.Completed).HasColumnName("Completed").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }

}

