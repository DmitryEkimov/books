﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace NavigationRoutes
{
    public class NavigationRouteFilter : INavigationRouteFilter
    {
        public bool ShouldRemove(Route route)
        {
            return true;
        }
    }
    public class AdminNavigationRouteFilter : INavigationRouteFilter
    {
        private string _username;
        public AdminNavigationRouteFilter(string username)
        {
            _username = username;
        }
        public bool ShouldRemove(Route route)
        {

            if (route is NamedRoute)
            {
                if (HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated)
                    return true;
                var nr = route as NamedRoute;
                //if (nr.DisplayName == "Книги" )
                //    return HttpContext.Current.User!=null && HttpContext.Current.User.Identity.IsAuthenticated;
                if (nr.DisplayName == "Счета" || nr.DisplayName == "Договоры" || nr.DisplayName == "Чеки")
                    return !HttpContext.Current.User.IsInRole("администратор") && !HttpContext.Current.User.IsInRole("редактор");
                if (nr.DisplayName == "Пользователи" )
                    return !HttpContext.Current.User.IsInRole("администратор");
            }
            return false;
        }
    }
}
