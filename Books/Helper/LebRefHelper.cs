﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text.RegularExpressions;
using System.Web;
using Books.DataAccess;

namespace Books.Helper
{
    public static class LebRefHelper
    {
        public static  string LibRef(this Works t)
        {
            var strYpp = YPP(t.Year, t.Publisher, t.Address, true);
            var strVolNum = VolNum(t.Volume, t.Number);
            var strres = "";
            //Разные типы описаний для записей разного типа
            switch (t.BibType)
            {
                case "book": //Книга
                case "inbook":
                    if (String.IsNullOrWhiteSpace(t.Author) && String.IsNullOrWhiteSpace(t.Year)) { strres = t.Title; break; }
                    var strTitle = !String.IsNullOrWhiteSpace(t.Editor)
                                          ? ChSp(t.Title, " /", true) + ChSp(pNoDot(t.Editor), "", true)
                                          : ChSp(t.Title, "", true);
                    strres = ChSp(t.Author, ".", true) + strTitle + ChSp(strYpp, ".", true) + ChSp(strVolNum, ".", true);
                    if (!String.IsNullOrWhiteSpace(t.Series)) strres = pNoDot(strres) + " (" + t.Series + ").";
                    break;
                case "article": //Журнальная статья
                    if (String.IsNullOrWhiteSpace(t.Journal)) { strres = t.Title; break; }
                    strres = pNoDot(ChSp(t.Author, ".") + t.Title) + " // " + ChSp(t.Journal, ".", true) + ChSp(strVolNum, ".", true) + ChSp(t.Year, ".", true);
                    if (!String.IsNullOrWhiteSpace(t.Pp)) strres = strres + ChSp(ProcessPages(t.Pp, t.Journal), ".", true);
                    break;
                case "incollection": //Книжная статья
                    if (String.IsNullOrWhiteSpace(t.Booktitle)) { strres = t.Title; break; }// Если библиографическое описание не расписано по полям
                    var strbooktitle = !String.IsNullOrWhiteSpace(t.Editor)
                                              ? ChSp(t.Booktitle, " /") + ChSp(pNoDot(t.Editor), "", true)
                                              : t.Booktitle;
                    strres = pNoDot(ChSp(t.Author, ".") + t.Title) + " // " + ChSp(strbooktitle, ".") + ChSp(strYpp, ".", true) + ChSp(strVolNum, ".", true);
                    if (!String.IsNullOrWhiteSpace(t.Series))
                        strres = pNoDot(strres) + " " + ChSp("(" + t.Series + ")", ".");
                    if (!String.IsNullOrWhiteSpace(t.Pp))
                        strres = strres + ChSp(ProcessPages(t.Pp, strbooktitle), ".", true);
                    break;
            }
            return strres.Trim().Replace('\r', ' ').Replace('\n', ' ').Replace("  ", " ").Replace("  ", " ");
        }
        private static string ChSp(string str1, string strChar, bool bln1let = false)
        {
            string str2 = !String.IsNullOrWhiteSpace(str1) ? (String.IsNullOrWhiteSpace(strChar) ? str1 + " " : (str1.Substring(Math.Max(str1.Length - strChar.Length, Math.Min(str1.Length, strChar.Length))) == strChar ? str1 + " " : str1 + strChar + " ")) : "";
            if (bln1let && str2.Length > 1) str2 = str2.Substring(0, 1).ToUpper() + str2.Substring(1);
            else if (bln1let) str2 = str2.ToUpper();
            return str2;
        }
        private static string pNoDot(string vinput)
        {
            var str1 = vinput.Trim();
            return (str1.Substring(str1.Length - 1) == ".") ? str1.Substring(0, str1.Length - 1) : str1;
        }
        private static string pDot(string vinput)
        {
            if (string.IsNullOrWhiteSpace(vinput)) return "";
            var str1 = vinput.Trim();
            return (str1.Substring(str1.Length - 1) == ".") ? str1 : str1 + ".";
        }
        private static string YPP(string strYear, string strPubl, string strPlace, bool blnNoPubl = false)
        {
            string str1 = "";
            if (String.IsNullOrWhiteSpace(strYear) && String.IsNullOrWhiteSpace(strPubl) &&
                String.IsNullOrWhiteSpace(strPlace)) return str1;
            if (String.IsNullOrWhiteSpace(strPubl) || blnNoPubl)
                str1 = String.IsNullOrWhiteSpace(strYear) ? pDot(strPlace) : ChSp(strPlace, ",", true) + pDot(strYear);
            else
            {
                str1 = ChSp(strPlace, ":", true);
                str1 = !String.IsNullOrWhiteSpace(strYear)
                           ? str1 + ChSp(strPubl, ",") + pDot(strYear)
                           : str1 + pDot(strPubl);
            }
            return str1;
        }
        private static string VolNum(string strVolume, string strNumber)
        {
            if (String.IsNullOrWhiteSpace(strVolume))
            {
                if (!String.IsNullOrWhiteSpace(strNumber)) return strNumber;
            }
            else
            {
                if (!String.IsNullOrWhiteSpace(strNumber)) return ChSp(strVolume, ", ") + Lower1st(strNumber);
                return strVolume;
            }
            return "";
        }
        private static string Lower1st(string str2)
        {
            return !String.IsNullOrWhiteSpace(str2) && str2.Length > 1 && str2.Substring(0, Math.Min(str2.Length, 3)) != "Bd." && str2.Substring(0, Math.Min(str2.Length, 4)) != "Heft" && str2.Substring(0, Math.Min(str2.Length, 3)) != "Hft" &&
                    str2.Substring(0, Math.Min(str2.Length, 3)) != "Jg." && str2.Substring(0, Math.Min(str2.Length, 4)) != "Jahr"
                    && str2.Substring(0, Math.Min(str2.Length, 2)) != "H." && str2.Substring(0, Math.Min(str2.Length, 4)) != "Teil" && str2.Substring(0, Math.Min(str2.Length, 5)) != "Absch"
                       ? str2.Substring(0, 1).ToLower() + str2.Substring(1)
                       : str2;
        }
        private static bool IsNumeric(string strTextEntry)
        {
            var objNotWholePattern = new Regex("[0-9]");
            return !string.IsNullOrWhiteSpace(strTextEntry) && objNotWholePattern.IsMatch(strTextEntry);
        }
        private static string ProcessPages(string strpg, string strTitle)
        {
            var strpages = strpg;
            var regex = new Regex("[а-я]*");
            if (IsNumeric(strpages.Substring(0, 1)))
                //Если в поле pages не указан тип страниц, необходимо добавить p или c (Автоматически определить немецкий, польский языки не получится)
                strpages = (!string.IsNullOrWhiteSpace(strTitle) && regex.IsMatch(strTitle.ToLower()))
                               ? "С. " + strpages
                               : "P. " + strpages;
            return strpages;
        }
    }
}