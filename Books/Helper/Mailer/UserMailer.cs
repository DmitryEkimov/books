﻿using System;
using System.Net.Mail;
using Books.DataAccess;
using FluentEmail;
using System.Linq;
using Books.Helper;

namespace Books.Mailer
{ 
    public class UserMailer : IUserMailer 	
	{
        private readonly string _from;
        private SmtpClient _smtpClient;
        protected readonly BooksDbContext _context;

        public UserMailer(BooksDbContext context, SmtpClient smtpClient)
		{
            _context = context;
            if (smtpClient == null) throw new ArgumentNullException("smtpClient");
            _from = "postmaster@app7949.mailgun.org";//will move to web.config
            _smtpClient = smtpClient;
		}

        public void SendЗаказано(string email, int BookId)
        {
            var book = _context.Works.Single(b => b.Id == BookId);
            //книга (приводится описание книги), на которую он оставлял заявку, заказана в Интернет-магазине (указывается магазин, OrderID, дата заказа). 
            var template = "Книга " + book.LibRef() + "\r\n,на которую Вы оставляли заявку, заказана \r\n" +
                           "в Интернет-магазине " + book.Seller + ", код заказа: " + book.OrderId + ", дата заказа: " +
                           (book.Ordered.HasValue? book.Ordered.Value.ToShortDateString(): "");
            Email
            .From(_from)
            .To(email)
            .Subject("Ваша Книга заказана")
            .Body(template,false)
            .UsingClient(_smtpClient)
            .Send();
		}

        public void SendПолучено(string email, int BookId)
		{
            var book = _context.Works.Single(b => b.Id == BookId);
            var template = "Книга " + book.LibRef() + "\r\n,на которую Вы оставляли заявку, получена \r\n";
            Email
            .From(_from)
            .To(email)
            .Subject("Ваша Книга получена")
            .Body(template,false)
            .UsingClient(_smtpClient)
            .Send();
		}

        public void SendОбработано(string email, int BookId)
        {
            //книга доступна в электронной библиотеке, в письме указывается значение поля Path.
            var book = _context.Works.Single(b => b.Id == BookId);
            //книга (приводится описание книги), на которую он оставлял заявку, заказана в Интернет-магазине (указывается магазин, OrderID, дата заказа). 
            var template = "Книга " + book.LibRef() + "\r\n,на которую Вы оставляли заявку, доступна в электронной библиотеке \r\n" +
                           "по адресу " + book.Path;
            Email
            .From(_from)
            .To(email)
            .Subject("Ваша Книга обработана")
            .Body(template, false)
            .UsingClient(_smtpClient)
            .Send();
        }
 	}
}