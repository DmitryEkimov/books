﻿namespace Books.Mailer
{ 
	public interface IUserMailer
	{
        void SendЗаказано(string email, int BookId);
        void SendПолучено(string email, int BookId);
        void SendОбработано(string email, int BookId);
	}
}