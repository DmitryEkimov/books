﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace Books.Helper
{

    public class DecimalModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext,
                                ModelBindingContext bindingContext)
        {
            var valueResult = bindingContext.ValueProvider
                                            .GetValue(bindingContext.ModelName);
            var modelState = new ModelState {Value = valueResult};
            object actualValue = null;
            try
            {
                decimal val;
                NumberFormatInfo numberInfo = CultureInfo.CurrentCulture.NumberFormat;
                //if(decimal.TryParse(valueResult.AttemptedValue, 
                //    NumberStyles.AllowCurrencySymbol|NumberStyles.AllowDecimalPoint|NumberStyles.AllowLeadingSign|NumberStyles.AllowLeadingWhite|NumberStyles.AllowThousands|NumberStyles.AllowTrailingWhite,
                //    numberInfo,out val)

                actualValue = Convert.ToDecimal(valueResult.AttemptedValue, CultureInfo.CurrentCulture);
            }
            catch (FormatException e)
            {
                modelState.Errors.Add(e);
            }

            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
            return actualValue;
        }
    }
}