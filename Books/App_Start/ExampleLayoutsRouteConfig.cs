﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Providers.Entities;
using System.Web.Routing;
using Books.Controllers;
using BootstrapMvcSample.Controllers;
using NavigationRoutes;

namespace BootstrapMvcSample
{
    public class ExampleLayoutsRouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
                routes.MapNavigationRoute<BooksController>("Книги", c => c.Index());
                routes.MapNavigationRoute<UsersController>("Пользователи", c => c.Index());

                routes.MapNavigationRoute<InvoicesController>("Счета", c => c.Index());
                routes.MapNavigationRoute<ContractsController>("Договоры", c => c.Index());
                routes.MapNavigationRoute<ChecksController>("Чеки", c => c.Index());


        }
    }
}
