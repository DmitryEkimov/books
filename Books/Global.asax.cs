﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Books.DataAccess;
using Books.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebMatrix.WebData;

namespace Books
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Error(Object sender, System.EventArgs ev)
        {
            System.Web.HttpContext context = HttpContext.Current;
            System.Exception ex = Context.Server.GetLastError();

            //LogError(e); //Your own logging function...

            context.Server.ClearError();

            Response.Redirect("/");

        }

        protected void Application_Start()
        {
            WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", false);
            //WebSecurity.CreateUserAndAccount("Громова", "Громова");
            //WebSecurity.CreateUserAndAccount("Ильин-Томич", "Ильин-Томич");

            AreaRegistration.RegisterAllAreas();
            ModelBinders.Binders.Add(typeof(Datatables.Mvc.DataTable), new Datatables.Mvc.DataTableModelBinder());
            ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());
            var formatter = GlobalConfiguration.Configuration.Formatters.FirstOrDefault(f => f.SupportedMediaTypes.Any(v => v.MediaType.Equals("application/json", StringComparison.CurrentCultureIgnoreCase))) as JsonMediaTypeFormatter;
            if (formatter != null)
            {
                formatter.SerializerSettings = new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    CheckAdditionalContent = false,
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateParseHandling = DateParseHandling.DateTime,
                    PreserveReferencesHandling = PreserveReferencesHandling.None,
                    TypeNameHandling = TypeNameHandling.None,
                    //ReferenceResolver
                    //ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    Error = delegate(object sender, ErrorEventArgs args)
                    {
                        //errors.Add(args.ErrorContext.Error.Message);
                        args.ErrorContext.Handled = true;
                    }
                    //DateTimeZoneHandling = 
                    //Culture = CultureInfo.CurrentCulture
                    //Converters = new JsonConverter[] { new IsoDateTimeConverter() }
                    // whatever else you need...
                };
            }
            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
            //FormatterConfig.RegisterFormatters();

            var builder = new ContainerBuilder();
            builder.RegisterModule<DataAccessModule>();
            builder.RegisterModule<WebModule>();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            BootstrapSupport.BootstrapBundleConfig.RegisterBundles(System.Web.Optimization.BundleTable.Bundles);
            BootstrapMvcSample.ExampleLayoutsRouteConfig.RegisterRoutes(RouteTable.Routes);
            //FluentValidationModelValidatorProvider.Configure();
        }
    }
}