﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Books.DataAccess.Helpers;
using Books.DataAccess.Models;

namespace Books.DataAccess
{
    public class BooksDbContext : DbContext
    {
        public IDbSet<Acts> Acts { get; set; } // acts
        public IDbSet<Addresses> Addresses { get; set; } // addresses
        public IDbSet<Authors> Authors { get; set; } // authors
        public IDbSet<BibTypes> BibTypes { get; set; } // BibTypes
        public IDbSet<Checks> Checks { get; set; } // checks
        public IDbSet<Contracts> Contracts { get; set; } // contracts
        public IDbSet<Currencies> Currencies { get; set; } // Currencies
        public IDbSet<Formats> Formats { get; set; } // Formats
        public IDbSet<Invoices> Invoices { get; set; } // invoices
        public IDbSet<Journals> Journals { get; set; } // journals
        public IDbSet<OrderTypes> OrderTypes { get; set; } // OrderTypes
        public IDbSet<Payments> Payments { get; set; } // payments
        public IDbSet<Persons> Persons { get; set; } // persons
        public IDbSet<Publishers> Publishers { get; set; } // publishers
        //public IDbSet<QArtOrder> QArtOrder { get; set; } // qArtOrder
        //public IDbSet<QBookOrder> QBookOrder { get; set; } // qBookOrder
        //public IDbSet<QInbookOrder> QInbookOrder { get; set; } // qInbookOrder
        //public IDbSet<QIncollectionOrder> QIncollectionOrder { get; set; } // qIncollectionOrder
        //public IDbSet<QryPagesByStatus> QryPagesByStatus { get; set; } // qryPagesByStatus
        public IDbSet<Sellers> Sellers { get; set; } // sellers
        public IDbSet<Statuses> Statuses { get; set; } // Statuses
        //public IDbSet<Sysdiagrams> Sysdiagrams { get; set; } // sysdiagrams
        //public IDbSet<TblSearchClean> TblSearchClean { get; set; } // tblSearchClean
        //public IDbSet<TblSearchResults> TblSearchResults { get; set; } // tblSearchResults
        public IDbSet<Themes> Themes { get; set; } // themes
        public IDbSet<UserProfile> UserProfiles { get; set; } // users
        public IDbSet<Works> Works { get; set; } // works
        //public IDbSet<WorkTest> WorkTest { get; set; } // work_test
        public IDbSet<Membership> Membership { get; set; }
        public IDbSet<Role> Roles { get; set; }
        public IDbSet<OAuthMembership> OAuthMembership { get; set; }
        public IDbSet<ChecksPrevNext> ChecksPrevNext { get; set; }
       
        public IDbSet<InvoicesPrevNext> InvoicesPrevNext { get; set; }
        public IDbSet<WorksPrevNext> WorksPrevNext { get; set; }
        public IDbSet<ContractsPrevNext> ContractsPrevNext { get; set; }
        public IDbSet<UserProfilesPrevNext> UserProfilesPrevNext { get; set; }
        static BooksDbContext()
        {
            DbContextUtils<BooksDbContext>.SetInitializer(null);

            //var configuration = new Books.DataAccess.Migrations.Configuration();
            //var migrator = new DbMigrator(configuration);
            //migrator.Update();
            //if (ConfigurationManager.AppSettings["NeedToCreateDB"].ToLower() == "true")
            //    DbContextUtils<<BooksDbContext>.SetInitializer(new ReservRoomsDBInitializer());
            //else
                // don't validate the schema
        }

        public BooksDbContext()
            : base("Name=DefaultConnection")
        {
        }

        public BooksDbContext(string connectionString)
            : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ActsConfiguration());
            modelBuilder.Configurations.Add(new AddressesConfiguration());
            modelBuilder.Configurations.Add(new AuthorsConfiguration());
            modelBuilder.Configurations.Add(new BibTypesConfiguration());
            modelBuilder.Configurations.Add(new ChecksConfiguration());
            modelBuilder.Configurations.Add(new ContractsConfiguration());
            modelBuilder.Configurations.Add(new CurrenciesConfiguration());
            modelBuilder.Configurations.Add(new FormatsConfiguration());
            modelBuilder.Configurations.Add(new InvoicesConfiguration());
            modelBuilder.Configurations.Add(new JournalsConfiguration());
            modelBuilder.Configurations.Add(new OrderTypesConfiguration());
            modelBuilder.Configurations.Add(new PaymentsConfiguration());
            modelBuilder.Configurations.Add(new PersonsConfiguration());
            modelBuilder.Configurations.Add(new PublishersConfiguration());
            //modelBuilder.Configurations.Add(new QArtOrderConfiguration());
            //modelBuilder.Configurations.Add(new QBookOrderConfiguration());
            //modelBuilder.Configurations.Add(new QInbookOrderConfiguration());
            //modelBuilder.Configurations.Add(new QIncollectionOrderConfiguration());
           // modelBuilder.Configurations.Add(new QryPagesByStatusConfiguration());
            modelBuilder.Configurations.Add(new SellersConfiguration());
            modelBuilder.Configurations.Add(new StatusesConfiguration());
            //modelBuilder.Configurations.Add(new SysdiagramsConfiguration());
            //modelBuilder.Configurations.Add(new TblSearchCleanConfiguration());
            //modelBuilder.Configurations.Add(new TblSearchResultsConfiguration());
            modelBuilder.Configurations.Add(new ThemesConfiguration());
            modelBuilder.Configurations.Add(new UserProfileConfiguration());
            modelBuilder.Configurations.Add(new WorksConfiguration());
            //modelBuilder.Configurations.Add(new WorkTestConfiguration());
            modelBuilder.Configurations.Add(new OAuthMembershipConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Configurations.Add(new MembershipConfiguration());
            modelBuilder.Configurations.Add(new ChecksPrevNextConfiguration());
            modelBuilder.Configurations.Add(new ContractsPrevNextConfiguration());
            modelBuilder.Configurations.Add(new InvoicesPrevNextConfiguration());
            modelBuilder.Configurations.Add(new WorksPrevNextConfiguration());
            modelBuilder.Configurations.Add(new UserProfilesPrevNextConfiguration());
        }
    }
}
