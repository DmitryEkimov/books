﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // BibTypes
    public class BibTypes
    {
        public string BibType { get; set; } // bibType (Primary key)
        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_act;
    }

    // BibTypes
    internal class BibTypesConfiguration : EntityTypeConfiguration<BibTypes>
    {
        public BibTypesConfiguration()
        {
            ToTable("dbo.BibTypes");
            HasKey(x => x.BibType);

            Property(x => x.BibType).HasColumnName("bibType").IsRequired().HasMaxLength(12);
        }
    }
}
