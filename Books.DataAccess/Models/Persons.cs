﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // persons
    public class Persons
    {
        public int Id { get; set; } // ID (Primary key)
        public string Name { get; set; } // Name
        public string Surname { get; set; } // Surname
        public string Comm { get; set; } // Comm
    }
    // persons
    internal class PersonsConfiguration : EntityTypeConfiguration<Persons>
    {
        public PersonsConfiguration()
        {
            ToTable("dbo.persons");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(255);
            Property(x => x.Surname).HasColumnName("Surname").IsOptional().HasMaxLength(255);
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }
}
