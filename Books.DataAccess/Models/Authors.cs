﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // authors
    public class Authors
    {
        public int Id { get; set; } // ID (Primary key)
        public int? PersonId { get; set; } // personID
        public int? Work { get; set; } // Work
        public string Role { get; set; } // Role
        public short? Index { get; set; } // index
        public string Comm { get; set; } // Comm
    }

    // authors
    internal class AuthorsConfiguration : EntityTypeConfiguration<Authors>
    {
        public AuthorsConfiguration()
        {
            ToTable("dbo.authors");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.PersonId).HasColumnName("personID").IsOptional();
            Property(x => x.Work).HasColumnName("Work").IsOptional();
            Property(x => x.Role).HasColumnName("Role").IsOptional().HasMaxLength(100);
            Property(x => x.Index).HasColumnName("index").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional();
        }
    }
}
