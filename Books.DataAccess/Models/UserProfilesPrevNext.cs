﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess.Models
{
    public class UserProfilesPrevNext
    {
        public short? PreviousRow { get; set; } // ID (Primary key)
        public short Id { get; set; } // ID (Primary key)
        public short? NextRow { get; set; } // ID (Primary key)
        public long RN { get; set; }
    }
        // works
    internal class UserProfilesPrevNextConfiguration : EntityTypeConfiguration<UserProfilesPrevNext>
    {
        public UserProfilesPrevNextConfiguration()
        {
            ToTable("dbo.UserProfilesPrevNext");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired();
            Property(x => x.PreviousRow).HasColumnName("PreviousRow").IsOptional();
            Property(x => x.NextRow).HasColumnName("NextRow").IsOptional();
            Property(x => x.RN).HasColumnName("RN").IsRequired();
        }
    }
}
