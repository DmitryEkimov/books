﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // users
    public class UserProfile
    {
        public short UserId { get; set; }  //(Primary key)
        public string UserName { get; set; }
        public string EMail { get; set; }
    }
    // users
    internal class UserProfileConfiguration : EntityTypeConfiguration<UserProfile>
    {
        public UserProfileConfiguration()
        {
            ToTable("dbo.UserProfile");
            HasKey(x => x.UserId);

            Property(x => x.UserId).HasColumnName("UserId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.UserName).HasColumnName("UserName").IsOptional().HasMaxLength(90);
            Property(x => x.EMail).HasColumnName("EMail").IsOptional().HasMaxLength(90);
        }
    }
}
