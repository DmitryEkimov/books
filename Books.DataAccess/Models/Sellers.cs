﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // sellers
    public class Sellers
    {
        public int Id { get; set; } // ID (Primary key)
        public string Name { get; set; } // Name
        public string Site { get; set; } // site
        public string EMail { get; set; } // e-mail
        public string Comm { get; set; } // Comm
    }
    // sellers
    internal class SellersConfiguration : EntityTypeConfiguration<Sellers>
    {
        public SellersConfiguration()
        {
            ToTable("dbo.sellers");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasMaxLength(255);
            Property(x => x.Site).HasColumnName("site").IsOptional().HasMaxLength(255);
            Property(x => x.EMail).HasColumnName("e_mail").IsOptional().HasMaxLength(255);
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }
}
