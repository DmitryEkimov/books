﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // payments
    public class Payments
    {
        public short Id { get; set; } // ID (Primary key)
        public string Code { get; set; } // Code
        public string Explanation { get; set; } // Explanation
    }
    // payments
    internal class PaymentsConfiguration : EntityTypeConfiguration<Payments>
    {
        public PaymentsConfiguration()
        {
            ToTable("dbo.payments");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Code).HasColumnName("Code").IsOptional().HasMaxLength(100);
            Property(x => x.Explanation).HasColumnName("Explanation").IsOptional().HasMaxLength(100);
        }
    }
}
