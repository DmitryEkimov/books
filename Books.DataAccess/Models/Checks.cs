﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // checks
    public class Checks
    {
        public int Id { get; set; } // ID (Primary key)
        public string File { get; set; } // File
        public DateTime? Date { get; set; } // Date
        public int? Num { get; set; } // Num
        public string Agency { get; set; } // Agency
        public decimal? Value { get; set; } // Value
        public int? ExtractNum { get; set; } // ExtractNum
        public DateTime? DateWithdrawal { get; set; } // DateWithdrawal
        public string Payer { get; set; } // Payer
        public short? Offset { get; set; } // offset
        public DateTime? ReportedOn { get; set; } // ReportedOn
        public bool IsPaid { get; set; } // ReportedOn
        public byte[] FileCopy { get; set; }
        public string ContentType { get; set; }
        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_check;

        public Checks()
        {
            Works = new List<Works>();
        }
    }

    // checks
    internal class ChecksConfiguration : EntityTypeConfiguration<Checks>
    {
        public ChecksConfiguration()
        {
            ToTable("dbo.checks");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Date).HasColumnName("Date").IsOptional();
            Property(x => x.Num).HasColumnName("Num").IsOptional();
            Property(x => x.Agency).HasColumnName("Agency").IsOptional().HasMaxLength(200);
            Property(x => x.Value).HasColumnName("Value").IsOptional();
            Property(x => x.ExtractNum).HasColumnName("ExtractNum").IsOptional();
            Property(x => x.DateWithdrawal).HasColumnName("DateWithdrawal").IsOptional();
            Property(x => x.Payer).HasColumnName("Payer").IsOptional().HasMaxLength(90);
            Property(x => x.Offset).HasColumnName("offset").IsOptional();
            Property(x => x.ReportedOn).HasColumnName("ReportedOn").IsOptional();
            Property(x => x.IsPaid).HasColumnName("IsPaid").IsRequired();
            Property(x => x.FileCopy).HasColumnName("FileCopy").IsOptional();
            Property(x => x.ContentType).HasColumnName("ContentType").HasMaxLength(50).IsOptional();
        }
    }
}
