﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess.Models
{
    //[Table("webpages_Membership")]
    public class Membership
    {
        public Membership()
        {
            Roles = new List<Role>();
            OAuthMemberships = new List<OAuthMembership>();
        }

        //[Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }
        public DateTime? CreateDate { get; set; }
        //[StringLength(128)]
        public string ConfirmationToken { get; set; }
        public bool? IsConfirmed { get; set; }
        public DateTime? LastPasswordFailureDate { get; set; }
        public int PasswordFailuresSinceLastSuccess { get; set; }
        //[Required, StringLength(128)]
        public string Password { get; set; }
        public DateTime? PasswordChangedDate { get; set; }
        //[Required, StringLength(128)]
        public string PasswordSalt { get; set; }
        //[StringLength(128)]
        public string PasswordVerificationToken { get; set; }
        public DateTime? PasswordVerificationTokenExpirationDate { get; set; }

        public ICollection<Role> Roles { get; set; }

        [ForeignKey("UserId")]
        public ICollection<OAuthMembership> OAuthMemberships { get; set; }
    }

    // acts
    internal class MembershipConfiguration : EntityTypeConfiguration<Membership>
    {
        public MembershipConfiguration()
        {
            ToTable("webpages_Membership");
            HasKey(x => x.UserId);

            Property(x => x.UserId).HasColumnName("UserId").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CreateDate).HasColumnName("CreateDate").IsOptional();
            Property(x => x.ConfirmationToken).HasColumnName("ConfirmationToken").HasMaxLength(128).IsOptional();
            Property(x => x.IsConfirmed).HasColumnName("IsConfirmed").IsOptional();
            Property(x => x.LastPasswordFailureDate).HasColumnName("LastPasswordFailureDate").IsOptional();
            Property(x => x.PasswordFailuresSinceLastSuccess).HasColumnName("PasswordFailuresSinceLastSuccess").IsRequired();
            Property(x => x.Password).HasColumnName("Password").HasMaxLength(128).IsRequired();
            Property(x => x.PasswordChangedDate).HasColumnName("PasswordChangedDate").IsOptional();
            Property(x => x.PasswordSalt).HasColumnName("PasswordSalt").HasMaxLength(128).IsRequired();
            Property(x => x.PasswordVerificationToken).HasColumnName("PasswordVerificationToken").HasMaxLength(128).IsOptional();
            Property(x => x.PasswordVerificationTokenExpirationDate).HasColumnName("PasswordVerificationTokenExpirationDate").IsOptional();
            HasMany(r => r.Roles).WithMany(u => u.Members).Map(m =>
            {
                m.ToTable("webpages_UsersInRoles");
                m.MapLeftKey("UserId");
                m.MapRightKey("RoleId");
            });
        }
    }
}
