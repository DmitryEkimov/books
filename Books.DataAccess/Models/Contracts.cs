﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // contracts
    public class Contracts
    {
        public int Id { get; set; } // ID (Primary key)
        public DateTime? Date { get; set; } // Date
        public string Num { get; set; } // Num
        public string Partner { get; set; } // Partner
        public string Subject { get; set; } // Subject
        public decimal? Value { get; set; } // Value
        public DateTime? Expire { get; set; } // Expire
        public string Comm { get; set; } // Comm
        public byte[] FileCopy { get; set; }
        public string ContentType { get; set; }
        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_contract;

        public Contracts()
        {
            Works = new List<Works>();
        }
    }

    // contracts
    internal class ContractsConfiguration : EntityTypeConfiguration<Contracts>
    {
        public ContractsConfiguration()
        {
            ToTable("dbo.contracts");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Date).HasColumnName("Date").IsOptional();
            Property(x => x.Num).HasColumnName("Num").IsOptional().HasMaxLength(90);
            Property(x => x.Partner).HasColumnName("Partner").IsOptional();
            Property(x => x.Subject).HasColumnName("Subject").IsOptional();
            Property(x => x.Value).HasColumnName("Value").IsOptional();
            Property(x => x.Expire).HasColumnName("Expire").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
            Property(x => x.FileCopy).HasColumnName("FileCopy").IsOptional();
            Property(x => x.ContentType).HasColumnName("ContentType").HasMaxLength(50).IsOptional();
        }
    }
}
