﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // addresses
    public class Addresses
    {
        public int Код { get; set; } // Код (Primary key)
        public string City { get; set; } // City
        public string Country { get; set; } // Country
    }

    // addresses
    internal class AddressesConfiguration : EntityTypeConfiguration<Addresses>
    {
        public AddressesConfiguration()
        {
            ToTable("dbo.addresses");
            HasKey(x => x.Код);

            Property(x => x.Код).HasColumnName("Код").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.City).HasColumnName("City").IsOptional();
            Property(x => x.Country).HasColumnName("Country").IsOptional();
        }
    }
}
