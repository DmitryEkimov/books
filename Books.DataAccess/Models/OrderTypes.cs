﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // OrderTypes
    public class OrderTypes
    {
        public string OrderType { get; set; } // OrderType (Primary key)

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_ordertype;

        public OrderTypes()
        {
            Works = new List<Works>();
        }
    }
    // OrderTypes
    internal class OrderTypesConfiguration : EntityTypeConfiguration<OrderTypes>
    {
        public OrderTypesConfiguration()
        {
            ToTable("dbo.OrderTypes");
            HasKey(x => x.OrderType);

            Property(x => x.OrderType).HasColumnName("OrderType").IsRequired().HasMaxLength(11);
        }
    }
}
