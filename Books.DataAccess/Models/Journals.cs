﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // journals
    public class Journals
    {
        public int Код { get; set; } // Код (Primary key)
        public string Name { get; set; } // Name
        public string Comm { get; set; } // Comm
    }
    // journals
    internal class JournalsConfiguration : EntityTypeConfiguration<Journals>
    {
        public JournalsConfiguration()
        {
            ToTable("dbo.journals");
            HasKey(x => x.Код);

            Property(x => x.Код).HasColumnName("Код").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName("Name").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional();
        }
    }
}
