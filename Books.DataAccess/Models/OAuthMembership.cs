﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess.Models
{
    //[Table("webpages_OAuthMembership")]
    public class OAuthMembership
    {
        //[Key, Column(Order = 0), StringLength(30)]
        public string Provider { get; set; }

        //[Key, Column(Order = 1), StringLength(100)]
        public string ProviderUserId { get; set; }

        public int UserId { get; set; }

        //[Column("UserId"), InverseProperty("OAuthMemberships")]
        public Membership User { get; set; }
    }
    internal class OAuthMembershipConfiguration : EntityTypeConfiguration<OAuthMembership>
    {
        public OAuthMembershipConfiguration()
        {
            ToTable("webpages_OAuthMembership");
            HasKey(a => new { a.Provider, a.ProviderUserId });

            Property(x => x.Provider).HasColumnName("Provider").HasMaxLength(30).IsRequired();
            Property(x => x.ProviderUserId).HasColumnName("ProviderUserId").HasMaxLength(100).IsRequired();
            Property(x => x.UserId).HasColumnName("UserId").IsRequired();
            HasRequired(c => c.User).WithMany(d => d.OAuthMemberships).HasForeignKey(c => c.UserId);
        }
    }

}
