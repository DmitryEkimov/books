﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Books.DataAccess
{
    // works
    public class Works
    {
        [ReadOnly(true)]
        [DisplayName("Код:")]
        public int Id { get; set; } // ID (Primary key)
        [ScaffoldColumn(false)]//[Display(AutoGenerateField = false)]
        public DateTime? CreatedOn { get; set; } // CreatedOn
        [DisplayName("Тип заказа:")]
        public string OrderType { get; set; } // OrderType
        [DisplayName("Тип записи:")]
        public string BibType { get; set; } // bibType
        [DisplayName("Носитель:")]
        public string Format { get; set; } // Format
        [DisplayName("Статус:")]
        public string Status { get; set; } // Status
        [DisplayName("Автор:")]
        public string Author { get; set; } // author
        [DisplayName("Загл:")]
        public string Title { get; set; } // title
        [DisplayName("Ред:")]
        public string Editor { get; set; } // editor
        [DisplayName("Серия:")]
        public string Series { get; set; } // series
        [DisplayName("Код ист-ка:")]
        public int? Source { get; set; } // source
        [DisplayName("Том:")]
        public string Volume { get; set; } // volume
        [DisplayName("Место:")]
        public string Address { get; set; } // address
        [DisplayName("Номер:")]
        public string Number { get; set; } // number
        [DisplayName("Изд-во:")]
        public string Publisher { get; set; } // publisher
        [DisplayName("Стр:")]
        public string Pp { get; set; } // pp
        [DisplayName("Год:")]
        public string Year { get; set; } // year
        [DisplayName("Глава:")]
        public string Chapter { get; set; } // chapter
        [DisplayName("ISBN")]
        public string Isbn { get; set; } // ISBN
        [DisplayName("Биб-ка")]
        public string Library { get; set; } // Library
        [DisplayName("Цена:")]
        public decimal? PrimaryPrice { get; set; } // PrimaryPrice
        [DisplayName("Скопированы сс.")]
        public string CopyRange { get; set; } // copyRange
        [DisplayName("Заказ:")]
        public DateTime? Ordered { get; set; } // Ordered
        [DisplayName("Торг-ц:")]
        public string Seller { get; set; } // seller
        [DisplayName("Число сс. на скан:")]
        public int? Count { get; set; } // count
        [DisplayName("Приход:")]
        public DateTime? Obtained { get; set; } // Obtained
        [DisplayName("Ссылка:")]
        public string Link { get; set; } // link
        [DisplayName("Формат:")]
        public string Size { get; set; } // size
        [DisplayName("Тема:")]
        public string Theme { get; set; } // theme
        [DisplayName("Исходный файл:")]
        public string File { get; set; } // File
        [DisplayName("Готовый файл:")]
        public string Path { get; set; } // Path
        [DisplayName("Доп.инф.:")]
        [UIHint("TextArea")]
        public string Comm { get; set; } // Comm

        [ScaffoldColumn(false)]
        public string User { get; set; } // user
        [ScaffoldColumn(false)]
        public string Booktitle { get; set; } // booktitle
        [ScaffoldColumn(false)]
        public string Journal { get; set; } // journal
        [ScaffoldColumn(false)]
        public string Currency { get; set; } // Currency
        [ScaffoldColumn(false)]
        public string OrderId { get; set; } // OrderID
        [ScaffoldColumn(false)]
        public string PostId { get; set; } // PostID
        [ScaffoldColumn(false)]
        public string History { get; set; } // history
        [ScaffoldColumn(false)]
        public int? Invoice { get; set; } // invoice
        [ScaffoldColumn(false)]
        public int? Contract { get; set; } // contract
        [ScaffoldColumn(false)]
        public int? Check { get; set; } // check
        [ScaffoldColumn(false)]
        public DateTime? Scanning { get; set; } // Scanning
        [ScaffoldColumn(false)]
        public DateTime? Scanned { get; set; } // Scanned
        [ScaffoldColumn(false)]
        public int? Act { get; set; } // Act
        [ScaffoldColumn(false)]
        public string WhereIs { get; set; } // WhereIs
        

        // Reverse navigation
        [ScaffoldColumn(false)]//
        public virtual ICollection<Works> Works2 { get; set; } // works.FK_works_works;

        // Foreign keys
        [ScaffoldColumn(false)]//
        public virtual Statuses Statuses { get; set; } //  Status - FK_works_status
        [ScaffoldColumn(false)]//
        public virtual OrderTypes OrderTypes { get; set; } //  OrderType - FK_works_ordertype
        [ScaffoldColumn(false)]//
        public virtual Formats Formats { get; set; } //  Format - FK_works_formats
        [ScaffoldColumn(false)]//
        public virtual Works Works1 { get; set; } //  Source - FK_works_works
        [ScaffoldColumn(false)]//
        public virtual Currencies Currencies { get; set; } //  Currency - FK_works_currency
        [ScaffoldColumn(false)]//
        public virtual Invoices Invoices { get; set; } //  Invoice - FK_works_invoice
        [ScaffoldColumn(false)]//
        public virtual Contracts Contracts { get; set; } //  Contract - FK_works_contract
        [ScaffoldColumn(false)]//
        public virtual Checks Checks { get; set; } //  Check - FK_works_check
        [ScaffoldColumn(false)]//
        public virtual Acts Acts { get; set; } //  Act - FK_works_act
        [ScaffoldColumn(false)]//
        public virtual BibTypes BibTypes { get; set; } //  Act - FK_works_act
        public Works()
        {
            Works2 = new List<Works>();
        }
    }

    // works
    internal class WorksConfiguration : EntityTypeConfiguration<Works>
    {
        public WorksConfiguration()
        {
            ToTable("dbo.works");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.CreatedOn).HasColumnName("CreatedOn").IsOptional();
            Property(x => x.Status).HasColumnName("Status").IsOptional().HasMaxLength(35);
            Property(x => x.User).HasColumnName("user").IsOptional().HasMaxLength(100);
            Property(x => x.Theme).HasColumnName("theme").IsOptional().HasMaxLength(100);
            Property(x => x.OrderType).HasColumnName("OrderType").IsOptional().HasMaxLength(11);
            Property(x => x.Library).HasColumnName("Library").IsOptional().HasMaxLength(6);
            Property(x => x.Format).HasColumnName("Format").IsOptional().HasMaxLength(10);
            Property(x => x.BibType).HasColumnName("bibType").IsOptional().HasMaxLength(12);
            Property(x => x.Author).HasColumnName("author").IsOptional().HasMaxLength(255);
            Property(x => x.Editor).HasColumnName("editor").IsOptional().HasMaxLength(255);
            Property(x => x.Title).HasColumnName("title").IsOptional();
            Property(x => x.Booktitle).HasColumnName("booktitle").IsOptional().HasMaxLength(255);
            Property(x => x.Publisher).HasColumnName("publisher").IsOptional().HasMaxLength(255);
            Property(x => x.Journal).HasColumnName("journal").IsOptional().HasMaxLength(255);
            Property(x => x.Year).HasColumnName("year").IsOptional().HasMaxLength(50);
            Property(x => x.Volume).HasColumnName("volume").IsOptional().HasMaxLength(50);
            Property(x => x.Number).HasColumnName("number").IsOptional().HasMaxLength(50);
            Property(x => x.Chapter).HasColumnName("chapter").IsOptional().HasMaxLength(255);
            Property(x => x.Pp).HasColumnName("pp").IsOptional().HasMaxLength(255);
            Property(x => x.Series).HasColumnName("series").IsOptional().HasMaxLength(255);
            Property(x => x.Address).HasColumnName("address").IsOptional().HasMaxLength(100);
            Property(x => x.Isbn).HasColumnName("ISBN").IsOptional().HasMaxLength(100);
            Property(x => x.Source).HasColumnName("source").IsOptional();
            Property(x => x.Seller).HasColumnName("seller").IsOptional().HasMaxLength(255);
            Property(x => x.Link).HasColumnName("link").IsOptional().HasMaxLength(255);
            Property(x => x.PrimaryPrice).HasColumnName("PrimaryPrice").IsOptional();
            Property(x => x.Currency).HasColumnName("Currency").IsOptional().HasMaxLength(3);
            Property(x => x.Ordered).HasColumnName("Ordered").IsOptional();
            Property(x => x.OrderId).HasColumnName("OrderID").IsOptional().HasMaxLength(255);
            Property(x => x.PostId).HasColumnName("PostID").IsOptional().HasMaxLength(255);
            Property(x => x.Obtained).HasColumnName("Obtained").IsOptional();
            Property(x => x.History).HasColumnName("history").IsOptional().HasMaxLength(255);
            Property(x => x.Invoice).HasColumnName("invoice").IsOptional();
            Property(x => x.Contract).HasColumnName("contract").IsOptional();
            Property(x => x.Check).HasColumnName("check").IsOptional();
            Property(x => x.CopyRange).HasColumnName("copyRange").IsOptional().HasMaxLength(255);
            Property(x => x.Count).HasColumnName("count").IsOptional();
            Property(x => x.Size).HasColumnName("size").IsOptional().HasMaxLength(2);
            Property(x => x.Scanning).HasColumnName("Scanning").IsOptional();
            Property(x => x.Scanned).HasColumnName("Scanned").IsOptional();
            Property(x => x.Act).HasColumnName("Act").IsOptional();
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Path).HasColumnName("Path").IsOptional().HasMaxLength(255);
            Property(x => x.WhereIs).HasColumnName("WhereIs").IsOptional().HasMaxLength(100);
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);

            // Foreign keys
            HasOptional(a => a.Statuses).WithMany(b => b.Works).HasForeignKey(c => c.Status); // FK_works_status
            HasOptional(a => a.OrderTypes).WithMany(b => b.Works).HasForeignKey(c => c.OrderType); // FK_works_ordertype
            HasOptional(a => a.Formats).WithMany(b => b.Works).HasForeignKey(c => c.Format); // FK_works_formats
            HasOptional(a => a.Works1).WithMany(b => b.Works2).HasForeignKey(c => c.Source); // FK_works_works
            HasOptional(a => a.Currencies).WithMany(b => b.Works).HasForeignKey(c => c.Currency); // FK_works_currency
            HasOptional(a => a.Invoices).WithMany(b => b.Works).HasForeignKey(c => c.Invoice); // FK_works_invoice
            HasOptional(a => a.Contracts).WithMany(b => b.Works).HasForeignKey(c => c.Contract); // FK_works_contract
            HasOptional(a => a.Checks).WithMany(b => b.Works).HasForeignKey(c => c.Check); // FK_works_check
            HasOptional(a => a.Acts).WithMany(b => b.Works).HasForeignKey(c => c.Act); // FK_works_act
            HasOptional(a => a.BibTypes).WithMany(b => b.Works).HasForeignKey(c => c.BibType); // FK_works_act
        }
    }
}
