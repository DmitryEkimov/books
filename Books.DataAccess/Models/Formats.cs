﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // Formats
    public class Formats
    {
        public string Format { get; set; } // Format (Primary key)

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_formats;

        public Formats()
        {
            Works = new List<Works>();
        }
    }
    // Formats
    internal class FormatsConfiguration : EntityTypeConfiguration<Formats>
    {
        public FormatsConfiguration()
        {
            ToTable("dbo.Formats");
            HasKey(x => x.Format);

            Property(x => x.Format).HasColumnName("Format").IsRequired().HasMaxLength(10);
        }
    }
}
