﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // Currencies
    public class Currencies
    {
        public string Currency { get; set; } // Currency (Primary key)

        // Reverse navigation
        public virtual ICollection<Invoices> Invoices { get; set; } // invoices.FK_invoices_currency;
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_currency;

        public Currencies()
        {
            Invoices = new List<Invoices>();
            Works = new List<Works>();
        }
    }
    // Currencies
    internal class CurrenciesConfiguration : EntityTypeConfiguration<Currencies>
    {
        public CurrenciesConfiguration()
        {
            ToTable("dbo.Currencies");
            HasKey(x => x.Currency);

            Property(x => x.Currency).HasColumnName("Currency").IsRequired().HasMaxLength(3);
        }
    }
}
