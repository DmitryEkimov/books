﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;

namespace Books.DataAccess
{
    // acts
    public class Acts
    {
        public int Id { get; set; } // ID (Primary key)
        public int? ActNum { get; set; } // ActNum
        public int? Contract { get; set; } // Contract
        public DateTime? Date { get; set; } // Date
        public decimal? Value { get; set; } // Value
        public string Comm { get; set; } // Comm

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_act;

        public Acts()
        {
            Works = new List<Works>();
        }
    }

    // acts
    internal class ActsConfiguration : EntityTypeConfiguration<Acts>
    {
        public ActsConfiguration()
        {
            ToTable("dbo.acts");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ActNum).HasColumnName("ActNum").IsOptional();
            Property(x => x.Contract).HasColumnName("Contract").IsOptional();
            Property(x => x.Date).HasColumnName("Date").IsOptional();
            Property(x => x.Value).HasColumnName("Value").IsOptional();
            Property(x => x.Comm).HasColumnName("Comm").IsOptional().HasMaxLength(255);
        }
    }

}
