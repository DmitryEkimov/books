﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // invoices
    public class Invoices
    {
        public int Id { get; set; } // ID (Primary key)
        public string File { get; set; } // File
        public string Seller { get; set; } // Seller
        public string Shop { get; set; } // Shop
        public string OrderNum { get; set; } // OrderNum
        public DateTime? Date { get; set; } // Date
        public decimal? TotalPrice { get; set; } // TotalPrice
        public string Currency { get; set; } // Currency
        public int? ExtractNum { get; set; } // ExtractNum
        public decimal? Eur { get; set; } // EUR
        public decimal? Rur { get; set; } // RUR
        public DateTime? DateWithdrawal { get; set; } // DateWithdrawal
        public DateTime? ReportedOn { get; set; } // ReportedOn
        public string RussianDesc { get; set; } // RussianDesc
        public int? AccountingId { get; set; } // AccountingID
        public bool NoInvoice { get; set; } // NoInvoice
        public byte[] FileCopy { get; set; }
        public string ContentType { get; set; }
        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_invoice;

        // Foreign keys
        public virtual Currencies Currencies { get; set; } //  Currency - FK_invoices_currency

        public Invoices()
        {
            Works = new List<Works>();
        }
    }
    // invoices
    internal class InvoicesConfiguration : EntityTypeConfiguration<Invoices>
    {
        public InvoicesConfiguration()
        {
            ToTable("dbo.invoices");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.File).HasColumnName("File").IsOptional().HasMaxLength(255);
            Property(x => x.Seller).HasColumnName("Seller").IsOptional().HasMaxLength(100);
            Property(x => x.Shop).HasColumnName("Shop").IsOptional().HasMaxLength(45);
            Property(x => x.OrderNum).HasColumnName("OrderNum").IsOptional().HasMaxLength(45);
            Property(x => x.Date).HasColumnName("Date").IsOptional();
            Property(x => x.TotalPrice).HasColumnName("TotalPrice").IsOptional();
            Property(x => x.Currency).HasColumnName("Currency").IsOptional().HasMaxLength(3);
            Property(x => x.ExtractNum).HasColumnName("ExtractNum").IsOptional();
            Property(x => x.Eur).HasColumnName("EUR").IsOptional();
            Property(x => x.Rur).HasColumnName("RUR").IsOptional();
            Property(x => x.DateWithdrawal).HasColumnName("DateWithdrawal").IsOptional();
            Property(x => x.ReportedOn).HasColumnName("ReportedOn").IsOptional();
            Property(x => x.RussianDesc).HasColumnName("RussianDesc").IsOptional();
            Property(x => x.AccountingId).HasColumnName("AccountingID").IsOptional();
            Property(x => x.NoInvoice).HasColumnName("NoInvoice").IsRequired();
            Property(x => x.FileCopy).HasColumnName("FileCopy").IsOptional();
            Property(x => x.ContentType).HasColumnName("ContentType").HasMaxLength(50).IsOptional();
            // Foreign keys
            HasOptional(a => a.Currencies).WithMany(b => b.Invoices).HasForeignKey(c => c.Currency); // FK_invoices_currency
        }
    }
}
