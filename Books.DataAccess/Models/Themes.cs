﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // themes
    public class Themes
    {
        public int Id { get; set; } // ID (Primary key)
        public string Title { get; set; } // Title
    }
    // themes
    internal class ThemesConfiguration : EntityTypeConfiguration<Themes>
    {
        public ThemesConfiguration()
        {
            ToTable("dbo.themes");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("ID").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Title).HasColumnName("Title").IsOptional().HasMaxLength(90);
        }
    }
}
