﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess
{
    // Statuses
    public class Statuses
    {
        public string Status { get; set; } // Status (Primary key)

        // Reverse navigation
        public virtual ICollection<Works> Works { get; set; } // works.FK_works_status;

        public Statuses()
        {
            Works = new List<Works>();
        }
    }
    // Statuses
    internal class StatusesConfiguration : EntityTypeConfiguration<Statuses>
    {
        public StatusesConfiguration()
        {
            ToTable("dbo.Statuses");
            HasKey(x => x.Status);

            Property(x => x.Status).HasColumnName("Status").IsRequired().HasMaxLength(35);
        }
    }
}
