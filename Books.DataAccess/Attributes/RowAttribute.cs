﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Books.DataAccess.Attributes
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class RowAttribute : System.Attribute
    {
        public int row;

        public RowAttribute(int row)
        {
            this.row = row;
        }
    }
}
