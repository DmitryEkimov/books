Namespace Bibliographic

    Public Module RefBuiling

        Public Function LibRef(RefData As RefData) As String

            Dim strBibType As String, strAuthor As String, strEditor As String, strTitle As String, strBooktitle As String, strPublisher As String
            Dim strJournal As String, strYear As String, strVolume As String, strNumber As String, strPages As String, strSeries As String, strAddress As String

            strBibType = RefData.BibType
            strAuthor = RefData.Author
            strEditor = RefData.Editor
            strTitle = RefData.Title
            strBooktitle = RefData.BookTitle
            strPublisher = RefData.Publisher
            strJournal = RefData.Journal
            strYear = RefData.Year
            strVolume = RefData.Volume
            strNumber = RefData.Number
            strPages = RefData.Pages
            strSeries = RefData.Series
            strAddress = RefData.Address




            Dim strYPP As String, strRes As String, strVolNum As String


            '������ ���� ��������, ��� ������� ������� ����
            Select Case strBibType
                Case "book" '�����

inb:
                    If strAuthor = "" And strYear = "" Then ' �������� �� ��������� �� �����
                        strRes = strTitle
                        GoTo fin
                    End If

                    If Not strEditor = "" Then strTitle = ChSp(strTitle, " /") & ChSp(pNoDot(strEditor), "", True)
                    strYPP = YPP(strYear, strPublisher, strAddress, True)
                    strVolNum = VolNum(strVolume, strNumber)
                    strRes = ChSp(ChSp(strAuthor, ".") & strTitle, ".", True) & ChSp(strYPP, ".", True) & ChSp(strVolNum, ".", True)
                    If Not strSeries = "" Then strRes = pNoDot(strRes) & " (" & strSeries & ")."

                Case "inbook"
                    GoTo inb

                Case "article" '���������� ������
                    If strJournal = "" Then  ' ���� ����������������� �������� �� ��������� �� �����
                        strRes = strTitle
                        GoTo fin
                    End If

                    strVolNum = VolNum(strVolume, strNumber)
                    strRes = pNoDot(ChSp(strAuthor, ".") & strTitle) & " // " & ChSp(strJournal, ".", True) & ChSp(strVolNum, ".", True) & ChSp(strYear, ".", True)
                    If Not strPages = "" Then
                        strRes = strRes & ChSp(ProcessPages(strPages, strJournal), ".", True)
                    End If

                Case "incollection" '������� ������
                    If strBooktitle = "" Then  ' ���� �� ��������� �� �����
                        strRes = strTitle
                        GoTo fin
                    End If

                    If Not strEditor = "" Then strBooktitle = ChSp(strBooktitle, " /") & ChSp(pNoDot(strEditor), "", True)
                    strYPP = YPP(strYear, strPublisher, strAddress, True)
                    strVolNum = VolNum(strVolume, strNumber)
                    strRes = pNoDot(ChSp(strAuthor, ".") & strTitle) & " // " & ChSp(strBooktitle, ".") & ChSp(strYPP, ".", True) & ChSp(strVolNum, ".", True)
                    If Not strSeries = "" Then strRes = pNoDot(strRes) & " " & ChSp("(" & strSeries & ")", ".")
                    If Not strPages = "" Then
                        strRes = strRes & ChSp(ProcessPages(strPages, strBooktitle), ".", True)
                    End If


            End Select
fin:
            strRes = Trim(strRes)
            strRes = Replace(strRes, vbNewLine, " ")
            strRes = Replace(strRes, vbCr, " ")
            strRes = Replace(strRes, vbLf, " ")
            strRes = Replace(strRes, "  ", " ")
            strRes = Replace(strRes, "  ", " ")

            Return Trim(strRes)

        End Function


        '��������� � ����� ������ str1 ���� strChar � ������.
        'bln1let = ��������� ������ ����� � �������   �������

        Public Function ChSp(str1 As String, strChar As String, Optional bln1let As Boolean = False) As String

            Dim input As String = str1
            Dim strRes As String
            Dim endingLen As Int32 = strChar.Length

            If input.Length > 0 Then
                If endingLen = 0 Then
                    strRes = input.Trim & " "
                ElseIf Right(input, endingLen) = strChar Then
                    strRes = Trim(input) & " "
                Else
                    strRes = Trim(input) & strChar & " "

                End If
            Else
                strRes = ""
            End If

            If bln1let And Len(strRes) > 1 Then
                strRes = UCase(Left(strRes, 1)) & Mid(strRes, 2)
            ElseIf bln1let Then
                strRes = UCase(strRes)
            End If

            Return strRes

        End Function


        '������� ����� � ����� ������, ���� ��� ��� ����
        Public Function pNoDot(vInput) As String
            Dim str1 As String
            str1 = IIf(vInput Is Nothing, "", Trim(vInput))
            If Right(str1, 1) = "." Then
                Return Left(str1, Len(str1) - 1)
            Else
                Return str1
            End If
        End Function

        '��������� ����: �����: ��-��, ���
        Public Function YPP(vY As String, vPub As String, vPl As String, Optional blnNoPubl As Boolean = False) As String
            Dim str1 As String
            Dim strYear As String
            Dim strPubl As String
            Dim strPlace As String

            strYear = IIf(vY Is Nothing, "", vY)
            strPubl = IIf(vPub Is Nothing, "", vPub)
            strPlace = IIf(vPl Is Nothing, "", vPl)

            If strPlace = "" And strPubl = "" And strYear = "" Then Return ""

            If strPubl = "" Or blnNoPubl Then
                If strYear <> "" Then
                    str1 = ChSp(strPlace, ",", True) + pDot(strYear)
                Else
                    str1 = pDot(strPlace)
                End If
            Else
                str1 = ChSp(strPlace, ":", True)
                If strYear <> "" Then
                    str1 = str1 & ChSp(strPubl, ",") & pDot(strYear)
                Else
                    str1 = str1 & pDot(strPubl)
                End If
            End If
            Return str1
        End Function


        '��������� ����� � ����� ������, ���� �� ��� ���
        Public Function pDot(vInput As String) As String
            Dim str1 As String
            str1 = IIf(vInput Is Nothing, "", vInput.Trim)
            If Len(str1) = 0 Then Return ""
            If Asc(Right(str1, 1)) = 46 Then
                Return str1
            Else
                Return str1 & "."
            End If
        End Function


        '��������� ���, �����
        Function VolNum(strVolume As String, strNumber As String) As String

            If strVolume = "" Then
                If Not strNumber = "" Then
                    Return strNumber
                Else
                    Return ""
                End If
            Else
                If Not strNumber = "" Then
                    VolNum = ChSp(strVolume, ", ") + Lower1st(strNumber)
                Else
                    VolNum = strVolume
                End If
            End If
        End Function


        '�������� � ������� �������� ������ ���� � ������, ����� ��������� �������� ����

        Function Lower1st(str2 As String) As String
            If Not (Left(str2, 3) = "Bd." Or Left(str2, 4) = "Heft" Or Left(str2, 3) = "Hft" Or Left(str2, 3) = "Jg." Or Left(str2, 4) = "Jahr" Or Left(str2, 2) = "H." Or Left(str2, 4) = "Teil" Or Left(str2, 5) = "Absch") Then
                Lower1st = LCase(Left(str2, 1)) & Mid(str2, 2)
            Else
                Lower1st = str2
            End If
        End Function


        '������������ ��������
        Function ProcessPages(strpg As String, strTitle As String) As String
            Dim strpages As String
            strpages = strpg
            If IsNumeric(Left(strpages, 1)) Then '���� � ���� pages �� ������ ��� �������, ��������� �. ��� �.
                If strTitle Like "*[�-�]*" Then
                    strpages = "�. " + strpages
                Else
                    strpages = "P. " & strpages
                End If
            End If
            strpages = Replace(strpages, " - ", ChrW(&H2013))
            strpages = Replace(strpages, "-", ChrW(&H2013))
            strpages = Replace(strpages, " " & ChrW(&H2014) & " ", ChrW(&H2013))
            strpages = Replace(strpages, ChrW(&H2014), ChrW(&H2013))
            Return strpages
        End Function



    End Module
End Namespace